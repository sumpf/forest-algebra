#!/usr/bin/python3
from formula import Formula
from nfta import NFTA

def test_ab_alternate_nfta():
    transitions = {
        ('q1','q0'):['q1'],
        ('q0','q1'):['q0']
    }
    init = {
        'a':['q0'],
        'b':['q1'],
    }
    states = ['q0','q1']
    alphabet = ['a', 'b']

    nfta = NFTA(alphabet, states, init, transitions)
    
    try:
        formula = Formula()
        formula._nfta = nfta
        insert_sequence = (
            ('ins_leaf', None, 'a', 0),
            ('ins_sub', 0, 'b', 1),
            ('ins_leaf', 1, 'b', 2, True),
            ('ins_sub', 2, 'a', 3),
            ('ins_sub', 3, 'b', 4),
            ('ins_sub', 4, 'a', 5),
            ('ins_leaf', 5, 'a', 6, False)
        )
        formula.run_operation_sequence(insert_sequence)
        formula.check_consistency()
        #formula._update_all_signatures(nfta)
        assert formula._get_roots()[0]._signatures == {('q1','q1')}
        formula.to_dot_graph("PASSED_test_ab_alternate_nfta")
    except Exception as error:
        formula.to_dot_graph("FAIL_test_ab_alternate_nfta")
        raise error

