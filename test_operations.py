#!/usr/bin/python3
from rotations import rotate
import node
from node import _InnerNode, _Leaf
from formula import Formula

def test_simple_delete_leaf_context():
    try:
        print('test_simple_delete_leaf_context')
        formula = Formula()
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_ab')
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        node_ab = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=leaf_b, operation='odot')
        leaf_a2 = _Leaf(formula, 'a', is_context=False, id='leaf_a')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_c')
        node_ac = _InnerNode(formula, is_context=False, left_child=leaf_a2, right_child=leaf_c, operation='oplus')
        node_abac = _InnerNode(formula, is_context=False, left_child=node_ab, right_child=node_ac, operation='odot')
        formula.root = node_abac
        formula._simple_delete_leaf(leaf_a2.id)
        formula._simple_delete_leaf(leaf_c.id)
        formula._simple_delete_leaf(leaf_b.id)

        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_simple_delete_leaf_context")
        raise error
    

def test_simple_delete_leaf():
    try:
        print('test_simple_delete_leaf')
        formula = Formula()
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        node_bc = _InnerNode(formula, is_context=True, left_child=leaf_b, right_child=leaf_c, operation='oplus')
        leaf_a = _Leaf(formula, 'a', is_context=False, id='leaf_aa')
        node_w = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=node_bc, operation='oplus')
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_v = _InnerNode(formula, is_context=False, left_child=node_w, right_child=leaf_d, operation='odot')
        formula.root = node_v

        formula._simple_delete_leaf(leaf_a.id)
    except Exception as error:
        formula.to_dot_graph("FAIL_test_simple_delete_leaf")
        raise error
