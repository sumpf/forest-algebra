#!/usr/bin/python3
from rotations import rotate
import node
from node import _InnerNode, _Leaf
from formula import Formula

def test_rotation1a():
    # test for rotation 1a
    try:
        print('test_output:creating rotation1a')
        formula = Formula()
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c_box', is_context=True, id='leaf_cc')
        node_bc = _InnerNode(formula, is_context=True, left_child=leaf_b, right_child=leaf_c, operation='odot')
        leaf_d = _Leaf(formula, 'd_box', is_context=True, id='leaf_dd')
        leaf_e = _Leaf(formula, 'e_box', is_context=True, id='leaf_ee')
        node_de = _InnerNode(formula, is_context=True, left_child=leaf_d, right_child=leaf_e, operation='odot')
        node_bcde = _InnerNode(formula, is_context=True, left_child=node_bc, right_child=node_de, operation='odot')
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_aa')
        node_root = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=node_bcde, operation='odot')
        formula.root = node_root

        assert (rotate(node_root))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot1a_after_rotation")
        raise error

def test_rotation1b():
    # test for rotation 1b
    try:
        print('test_output:creating rotation1b')
        formula = Formula()
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c_box', is_context=True, id='leaf_cc')
        node_bc = _InnerNode(formula, is_context=True, left_child=leaf_b, right_child=leaf_c, operation='odot')
        leaf_d = _Leaf(formula, 'd_box', is_context=True, id='leaf_dd')
        leaf_e = _Leaf(formula, 'e_box', is_context=True, id='leaf_ee')
        node_de = _InnerNode(formula, is_context=True, left_child=leaf_d, right_child=leaf_e, operation='odot')
        node_bcde = _InnerNode(formula, is_context=True, left_child=node_bc, right_child=node_de, operation='odot')
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_aa')
        node_root = _InnerNode(formula, is_context=True, left_child=node_bcde, right_child=leaf_a, operation='odot')
        formula.root = node_root

        assert (rotate(node_root))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot1b_after_rotation")
        raise error

def test_rotation1b_special():
    # test for rotation 1b special
    try:
        print('test_output:creating rotation1b_special')
        formula = Formula()
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c_box', is_context=True, id='leaf_cc')
        node_u = _InnerNode(formula, is_context=True, left_child=leaf_b, right_child=leaf_c, operation='odot')
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_aa')
        node_w = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=node_u, operation='odot')
        leaf_d = _Leaf(formula, 'd_box', is_context=True, id='leaf_dd')
        node_v = _InnerNode(formula, is_context=True, left_child=node_w, right_child=leaf_d, operation='odot')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot1bspec_after_rotation")
        raise error
        
def test_rotation2a():
    # test for rotation 2a
    try:
        print('test_output:creating rotation2a')
        formula = Formula()
        leaf_b = _Leaf(formula, 'b', is_context=False, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        node_u = _InnerNode(formula, is_context=False, left_child=leaf_b, right_child=leaf_c, operation='oplus')
        leaf_a = _Leaf(formula, 'a', is_context=False, id='leaf_aa')
        node_w = _InnerNode(formula, is_context=False, left_child=leaf_a, right_child=node_u, operation='oplus')
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_v = _InnerNode(formula, is_context=False, left_child=node_w, right_child=leaf_d, operation='oplus')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot2a_after_rotation")
        raise error

def test_rotation2b():
    # test for rotation 2b
    try:
        print('test_output:creating rotation2b')
        formula = Formula()
        leaf_b = _Leaf(formula, 'b', is_context=False, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        node_u = _InnerNode(formula, is_context=False, left_child=leaf_b, right_child=leaf_c, operation='oplus')
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_w = _InnerNode(formula, is_context=False, left_child=node_u, right_child=leaf_d, operation='oplus')
        leaf_a = _Leaf(formula, 'a', is_context=False, id='leaf_aa')
        node_v = _InnerNode(formula, is_context=False, left_child=leaf_a, right_child=node_w, operation='oplus')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot2b_after_rotation")
        raise error

def test_rotation3a():
    # test for rotation 3a
    try:
        print('test_output:creating rotation3a')
        formula = Formula()
        # all combinations of forest and context for leaf_b and leaf_c leads to correct outputs or Errors, if bc, w, v were not adapeted
        leaf_b = _Leaf(formula, 'b', is_context=False, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        node_bc = _InnerNode(formula, is_context=False, left_child=leaf_b, right_child=leaf_c, operation='oplus')
        # if leaf_a is not a context: correct Runtime Error
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_aa')
        node_w = _InnerNode(formula, is_context=False, left_child=leaf_a, right_child=node_bc, operation='odot')
        # is leaf_d and node_v are contexts: correct Assertion Error because no rotation is possible
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_v = _InnerNode(formula, is_context=False, left_child=node_w, right_child=leaf_d, operation='oplus')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot3a_after_rotation")
        raise error

def test_rotation3b():
    # test for rotation 3b
    try:
        print('test_output:creating rotation3b')
        formula = Formula()
        # if at least one of the leafs b and c is a context: correct Runtime Error (can not oplus two contexts)
        leaf_b = _Leaf(formula, 'b', is_context=False, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        node_bc = _InnerNode(formula, is_context=False, left_child=leaf_b, right_child=leaf_c, operation='oplus')
        # if leaf_a is not a context: correct Runtime Error 
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_aa')
        # if node_w is not a context: correct Runtime Error    
        node_w = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=node_bc, operation='oplus')
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_v = _InnerNode(formula, is_context=False, left_child=node_w, right_child=leaf_d, operation='odot')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot3b_after_rotation")
        raise error

def test_rotation3c():
    # test for rotation 3c
    try:
        print('test_output:creating rotation3c')
        formula = Formula()
        # if leaf_b is not a context: correct runtime error
        # if leaf_c is a context too: all error cases are detected correctly
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        node_bc = _InnerNode(formula, is_context=True, left_child=leaf_b, right_child=leaf_c, operation='oplus')
        # if leaf_a is a context too: correct Runtime Error
        leaf_a = _Leaf(formula, 'a', is_context=False, id='leaf_aa')
        # if node_w is not a context: correct Runtime Error    
        node_w = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=node_bc, operation='oplus')
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_v = _InnerNode(formula, is_context=False, left_child=node_w, right_child=leaf_d, operation='odot')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot3c_after_rotation")
        raise error

def test_rotation3d():
    # test for rotation 3d
    try:
        print('test_output:creating rotation3d')
        formula = Formula()
        # if leaf_c or leaf_d is a context and cd, w, v too: correct rotation. Otherwise correct runtime error
        # if both leafs are contexts: correct runtime error
        leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_cc')
        leaf_d = _Leaf(formula, 'd', is_context=False, id='leaf_dd')
        node_cd = _InnerNode(formula, is_context=False, left_child=leaf_c, right_child=leaf_d, operation='oplus')
        # if leaf_b is not a context: correct RuntimeError
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        node_w = _InnerNode(formula, is_context=False, left_child=leaf_b, right_child=node_cd, operation='odot')
        # if leaf_a is a context: correct RuntimeError
        leaf_a = _Leaf(formula, 'a', is_context=False, id='leaf_aa')
        node_v = _InnerNode(formula, is_context=False, left_child=leaf_a, right_child=node_w, operation='oplus')
        formula.root = node_v

        assert (rotate(node_v))
        formula.check_consistency()
    except Exception as error:
        formula.to_dot_graph("FAIL_test_output_rot3d_after_rotation")
        raise error
