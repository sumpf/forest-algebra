#!/usr/bin/python3
from collections import deque
from tree_node import ForestEnvironment
from copy import deepcopy
import random

class SequenceGenerator:
    """ Class for generating operation sequences with differend kinds of properties.

    Internally keeps a copied forest environment to track which nodes are leaves and always generate valid operations.
    """
    def __init__(self, forest_env=ForestEnvironment()):
        self.forest_env = deepcopy(forest_env)
        self._k_ary_insert_id_counter = 0

    def set_forest_env(self, forest_env):
        """ Sets a copy of the provided forest env for internal tracking of valid target ids.
        
        :param ForestEnvironment forest_env:
            ForestEnvironment which might be empty.
        """
        assert isinstance(forest_env, ForestEnvironment)
        self.forest_env = deepcopy(forest_env)

    def generate_random_sequence(self, length=100, **kwargs):
        """ Generates a valid sequence of operations as described in function run_operation_sequence.

        Probability of a random operation being an insert subdivision is 1 - del_prob - ins_leaf_prob.

        :param int length:
            Number of operations in the sequence.

        :param float del_prob:
            Probability of a random operation being a delete.

        :param float ins_leaf_prob:
            Probability of a random operation being an insert leaf.

        :return list:
            Generated sequence of operations.
        """
        op_sequence = []
        for i in range(0, length):
            op_sequence.append(self.generate_random_op(**kwargs))

        return op_sequence

    def get_random_leaf_target(self):
        if self.forest_env._nodes:
            return random.sample([leaf for leaf in self.forest_env._nodes.values()
                                      if not leaf.children
                                      and not leaf._is_virtual], 1)[0].id
        # else None is returned in order to create a root

    def get_random_node_target(self):
        if self.forest_env._nodes:
            return random.sample([node for node in self.forest_env._nodes.values() if not node._is_virtual], 1)[0].id
        # else None is returned in order to create a root

    def generate_random_op(self, del_prob=None, ins_leaf_prob=None, new_label=None, new_id=None, target_id=None):
        """ Generates a single valid operation with a random target.

        Default probabilities are 0 for del_prob, and (1-del_prob)/2 for ins_leaf and ins_sub.
        Probability of ins_sub is 1 - del_prob - ins_leaf_prob.
        
        :param float del_prob:
            Probability of a delete between 0 and 1.

        :param float ins_leaf_prob:
            Probability of an insert leaf operation between 0 and 1.

        :param str new_label:
            Label for the new node if an insert is generated.

        :param str new_id:
            Id for the new node if an insert is generated.

        :param str target_id:
            Target id if it should not be random.

        :return str:
            Generated valid operation.
        """
        #Updates the forest env
        if del_prob is not None and ins_leaf_prob is not None:
            assert del_prob + ins_leaf_prob <= 1

        if del_prob is None:
            del_prob = 0
        else:
            assert 0 <= del_prob and del_prob <= 1

        if ins_leaf_prob is None:
            ins_leaf_prob = (1 - del_prob)/2
        else:
            assert 0 <= ins_leaf_prob and ins_leaf_prob <= 1
            

        # choice of label for new node in case it is needed
        if new_label is None:
            new_label = 'a' if random.random() > 0.5 else 'b'
        
        # choice of op. in case no node exists a root will be created.
        r = random.random()
        if r < ins_leaf_prob or not self.forest_env._nodes:
            op_type = 'ins_leaf'
            target_id = self.get_random_node_target() if target_id is None else target_id
            insert_left = True if random.random() > 0.5 else False
            op = (op_type, target_id, new_label, new_id, insert_left)
        elif r < del_prob + ins_leaf_prob:
            op_type = 'delete'
            target_id = self.get_random_leaf_target() if target_id is None else target_id
            op = (op_type, target_id)
        else:
            op_type = 'ins_sub'
            target_id = self.get_random_node_target() if target_id is None else target_id
            op = (op_type, target_id, new_label, new_id)

        self.forest_env.run_operation_sequence([op])
        return op

    def generate_k_ary_insert(self, k=2, target_id=None):
        """ Generates a sequence of length k inserting 1 tree node as subdivision and k-1 neighbours.

        We cannot say much about the distribution yet.
        
        :param int k:
            k for k-aryness. k=2 generates binary trees. k > 0 is required.
            
        :param str target_id:
            Target id of the subdivision. If None is specified a random target will be chosen.

        :return list:
            Generated sequence as described in function run_operation_sequence.
        """
        assert k >= 1
        op_sequence = []
        self._k_ary_insert_id_counter += 1
        id_num = self._k_ary_insert_id_counter
        subdiv = self.generate_random_op(del_prob=0, ins_leaf_prob=0, target_id=target_id, new_id=f'__gen_k_ary_{str(id_num)}')
        op_sequence.append(subdiv)
        # as the create node of the subdivision only has 1 neighbour after the operation k-1 others need to be created
        
        for insert_number in range(0, k-1):
            ins_leaf = self.generate_random_op(ins_leaf_prob=1, target_id=f'__gen_k_ary_{str(id_num)}')
            op_sequence.append(ins_leaf)

        return op_sequence

    def generate_random_k_ary_tree_sequence(self, k=2, length=1):
        """ Generates a random sequence which results in a k-ary tree.

        :param int k:
            k for k-aryness. k=2 generates binary trees. k > 0 is required.

        :param int length:
            Length of the generated sequence. length % k needs to be 1 in order to create a valid k-ary tree.

        :return list: 
            Generated sequence as described in function run_operation_sequence.
        """
        if self.forest_env._nodes:
            raise RuntimeError('Must generate k-ary tree from scratch')

        if length % k != 1 and k > 1:
            raise RuntimeError(f'{length} % {k} != 1: insert sequence will not result in a k-ary tree')

        assert k >= 1

        # create root
        op_sequence = [self.generate_random_op()]        

        # create groups of subdiv/leaf inserts.
        for group_number in range(0, int((length - 1)/k)):
            op_sequence += self.generate_k_ary_insert(k=k)

        return op_sequence


    def generate_balanced_k_ary_tree_sequence(self, k=2, length=1):
        """ Generates a sequence that results in a balanced k-ary balanced tree. 

        Sequence is not random. "Balanced" means height of the c
        
        :param int k:
            k for k-aryness. k=2 generates binary trees. k > 0 is required.

        :param int length:
            Length of the generated sequence. length % k needs to be 1 in order to create a valid k-ary tree.

        :return list:
            Generated sequence as described in function run_operation_sequence.
        """
        
        if self.forest_env._nodes:
            raise RuntimeError('Must generate balanced tree from scratch')

        if length % k != 1 and k > 1:
            raise RuntimeError(f'{length} % {k} != 1: insert sequence will not result in a k-ary tree')
        
        # create root:
        op_sequence = [self.generate_random_op()]

        # generate layers of a tree until length is satisfied
        leaf_ids = []
        while(len(op_sequence) < length):
            #jump to next layer if needed
            if len(leaf_ids) < 1: 
                leaf_ids = [id for (id,tree_node) in self.forest_env._nodes.items() if not tree_node.children]

            op_sequence += self.generate_k_ary_insert(k=k, target_id=leaf_ids.pop(-1))
            
        return op_sequence

    def generate_bounded_height_tree(self, height=3, length=1, ins_leaf_prob=0.5):
        """ Generate a sequence as described in function run_operation_sequence that results in a tree of height k.
        
        :param int length:
            Length of the sequence.

        :param int height:
            Height bound of the generated tree.
        """

        if self.forest_env._nodes:
            raise RuntimeError('Must generate balanced tree from scratch')

        op_sequence = []
        for _ in range(0, length):
            # choose wether ins_leaf/subdivision is chosen
            r = random.random()
            if r < ins_leaf_prob or not self.forest_env._nodes: # insert leaf
                op_sequence.append(self.generate_random_op(ins_leaf_prob=1))
            else: # insert subdivision
                # we can only do a subdivision on nodes which dont have a longest path of height.
                all_heights = self.forest_env.get_height(get_all_heights=True)
                # calculate all nodes which are on longest paths
                longest_path_tree_nodes = set()
                queue = deque()
                next_layer = deque()
                for root in self.forest_env.get_roots():
                    if root._is_virtual:
                        # for virtual nodes consider the heights of the children instead
                        assert all_heights[root.id] <= height + 1
                        for child in root.children:
                            if all_heights[child.id] == height:
                                queue.append(child)
                    elif all_heights[root.id] == height:
                        queue.append(root)

                for current_height in range(height, 0, -1):
                    while queue:
                        front_of_queue = queue.popleft()
                        longest_path_tree_nodes.add(front_of_queue.id)
                        for child in front_of_queue.children:
                            # only children with maximum possible height are on a longest path
                            if all_heights[child.id] == current_height - 1:
                                next_layer.append(child)
                    queue = next_layer
                    next_layer = deque()

                # now find all nodes not on a longest path
                candidates = [node_id for (node_id, node) in self.forest_env._nodes.items()
                                  if node_id not in longest_path_tree_nodes
                                  and not node._is_virtual]
                if candidates:
                    target_id = random.sample(candidates, 1)[0]
                    op_sequence.append(self.generate_random_op(ins_leaf_prob=0, del_prob=0, target_id=target_id))
                else:
                    # case no subdivision is possible, generate insert leaf instead
                    op_sequence.append(self.generate_random_op(ins_leaf_prob=1))
        return op_sequence
