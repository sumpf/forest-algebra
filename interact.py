from formula import Formula
from tree_node import ForestEnvironment

global FOREST_ENV
global FORMULA
global SEQUENCE

def reset():
    global FOREST_ENV
    global FORMULA
    global SEQUENCE
    FOREST_ENV = ForestEnvironment()
    FORMULA = Formula()
    SEQUENCE = []
    FOREST_ENV.to_dot_graph('interactive_forest')
    FORMULA.to_dot_graph('interactive_formula')

def help():
    h = """
    availible Commands:
   
    reset
        Resets the current forest_environment

    help
        Show this text.

    store filename
        TODO

    load filename
        TODO

    ins_leaf target_id [new_label [new_id [insert_left]]]
        Inserts a new leaf node right of the target.

        target_id
           Id of the target where the new leaf should be inserted. None if the forest is empty and a root should be created.
        new_label
           Label the new leaf should get.
        new_id
           id the new leaf should get.
        insert_left
           Set to True to insert the leaf left of the target instead of right.

    ins_sub target_id [new_label [new_id]]
        Inserts a new tree node below target_node, all children of target tree node now become childern of the newtree node.

        target_id
           Id of the target where the new leaf should be inserted.
        new_label
           Label the new leaf should get.
        new_id
           id the new leaf should get.
    
    delete target_id
        Deletes the target if it is a leaf.

    target_id
        Id of the leaf that should be deleted.
    """
    print(h)

def operation(command, args):
    op = [command, None, None, None, 'False']
    for (i,arg) in enumerate(args):
        op[i+1] = arg
    if op[4] not in ['True', 'False']:
        print(f'insert_left has to be True or False but was {op[4]}.')
        return

    global FOREST_ENV
    global FORMULA
    global SEQUENCE
    try:
        FOREST_ENV.run_operation_sequence([op])
        FORMULA.run_operation_sequence([op])
        SEQUENCE.append([op])
        FOREST_ENV.to_dot_graph('interactive_forest')
        FORMULA.to_dot_graph('interactive_formula')
    except Exception as e:
        print(e)
        return
    print('done.')
    
        
def interactive():
    reset()
    while(True):
        user_input = input('Enter Command> ').split()
        command = user_input[0]
        args = user_input[1:]
        if command == 'reset':
            reset()
        elif command in ['ins_leaf','ins_sub','delete']:
            operation(command, args)
        else:
            help()
