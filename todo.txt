--- Done ---

x thin out unused imports
x make prints consistent/introduce logging
x document errors.py
x make Formular.root private
x make Node.id private
x document to_dot_graph
x test for insert leaf/insert subdivision target id not found fehler
x test simple delete invalid target id error/not leaf error
x test for relable
x test for delete bei wenig knoten für fall, dass target_parent_parent nicht existiert formular:331
x anschauen rotation3c/3d extra condition
CANC implement checks for invariants
CANC create main.py which can read operation sequences from file
x evaluation of formulars to signatures for evaluating NFTA
x evaluation of formulars to graphs
CANC evaluation of formulars to extended signatures for evaluating NFSTA
x fix formula spelling
x fix for ... in [self._nodes[id] for id ...] -> ...[self._nodes.values()]
x move common code from _Leaf and _InnerNode to _Node
x run_update_sequence -> run_operation_sequence
x docu: op_sequence described as sequence of updates -> .. of operations

--- TODO ---
add counting of unbalanced path length' to stats (dict length -> count)
add counting of rotations on heavily unbalanced nodes
add test whether special case in proof idea appears during a run of the adapted algorithm
keep track of the number of decendants for each node
renaming variable id
OPT refactor ugly code
document all classes ivars
prettyfy generated documentation
we need a way to track operations: implement counting of operations in Formular or look into profiling.
optimize delete: no bottom up search is required for finding the node of which the target node is right child, instead we can tell from the nodes parent right away:
    in case parent is oplus: target node is not the last leaf nothing needs to be changed
    in case parent is odot and target node is left child: target has to be context -> target is not a leaf so cannot be deleted
    in case parent is odot and target node is right child: no further bottum up search required
    
run_operation_sequence: handle case unknown operation

sigmod 2017
vansummeren
dynamic yannakakis

