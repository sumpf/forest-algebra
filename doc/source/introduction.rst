Introduction
------------

This project is a proof of concept of using forest-algebra formulas :ref:`https://dblp.org/rec/conf/lics/Niewerth18` as data structure. It accompanies the masters theses of Jonas Marasus of Sarah Kleest-Meißner.

It serves multiple purposes:

 - To reproduce the theoretical concept of forest-algebra formulas and accompanying algorithms in actual code
 - To visualize forest-algebra formulas
 - To provide a way to work with examples which exceed the dimensions of a piece of paper
 - To analyse parameters of forest-algebra formulas such as tree height and number of performed rotations.
 - To give some evidence the actual rotation count, tree heights and runtimes are much better than assumed in theory.
 
What this project does not try to do:

 - Provide a library for actual use in software
 - Provide in implementation statistical comparison to other data structures (this is only asymptotically optimized)
 
Usage: Interactive
------------------

Run "main.py interactive" to enter interactive mode. Two pdf files "interactive_forest.gv.pdf" and "interactive_formula.gv.pdf" will be created and can be viewed with any non-locking pdf viewer.

Type help for a list of commands. You can insert and delete nodes in the forest and watch how the corresponding formula changes.

Usage: Replicate Experiments
----------------------------

Run "main.py generate_simulations" to generate random forests and formula sequences the way they are described in Sarahs masters thesis. Run "main.py run_simulations" to run the simulations on these forests and formulas and output the statistics.

