#!/usr/bin/python3
"""Contains the class for handling Formula and public methods"""

from node import _Node, _Leaf, _InnerNode
from tree_node import TreeNode
from rotations import *
from errors import *
from copy import copy, deepcopy

from graphviz import Graph

REBALANCE_ALGORITHM_LIST = [
    'bottom_up_rebalance_once_sibling_niewerth'
]

class Formula:
    def __init__(self, nfta=None):
        """ Class for representing one or multiple forest algebra formulas.
        """
        self._leaf_id_counter = 0 # simple counter to assign a unique id to every new node if none is provided
        self._inner_node_id_counter = 0 #simple counter to assign a unique id to every new node if none is provided
        self._nodes = dict() # dict id -> node for all nodes
        self._leaves = dict() # dict id -> node for all leaves
        self._inner_nodes = dict() # dict id -> node for all inner nodes
        self._nfta = nfta
        self._rebalance_algorithm = 'bottom_up_rebalance_once_sibling_niewerth'
        # see set_operation_callback()
        def _operation_callback(func_name, callback_point, **kwargs):
            pass
        self._operation_callback = _operation_callback

    def __deepcopy__(self, memo):
        """ Creates a copy of the formula and all of its nodes.
        
        :param dict memo:
            See https://docs.python.org/3.6/library/copy.html#copy.deepcopy
        
        :return Formula:
            Copy of this formula.
        """
        # own deepcopy implementation is required as pythons deepcopy runs into stackoverflows on deep trees.
        new_formula = copy(self)
        new_formula._nodes = dict()
        new_formula._leaves = dict()
        new_formula._inner_nodes = dict()
        # copy all nodes
        for node_id, node in self._nodes.items():
            new_node = copy(node)
            new_node._signatures = deepcopy(node._signatures)
            new_node._formula = new_formula
            new_formula._nodes[node_id] = new_node
            if isinstance(new_node, _Leaf):
                new_formula._leaves[node_id] = new_node
            elif isinstance(new_node, _InnerNode):
                new_formula._inner_nodes[node_id] = new_node
            else:
                assert False

        # fix edges
        for new_node_id, new_node in new_formula._nodes.items():
            if isinstance(new_node, _InnerNode):
                new_node._left_child = new_formula._nodes[new_node._left_child.id]
                new_node._right_child = new_formula._nodes[new_node._right_child.id]
                assert new_node._left_child is not None and new_node._right_child is not None
            if new_node._parent is not None:
                new_node._parent = new_formula._nodes[new_node._parent.id]
        
        
        return new_formula

    def set_rebalance_algorithm(self, algorithm):
        """ Sets the rebalancing algorithm for this formula.

        :param str algorithm:
            possible algorithms:
                'bottom_up_rebalance_once_sibling_niewerth'
        """
        if algorithm in REBALANCE_ALGORITHM_LIST:
            self._rebalance_algorithm = algorithm
        else:
            raise RuntimeError('Invalid rebalance algorithm specified')

    def set_operation_callback(self, operation_callback):
        """ Sets a callback function which is called in multiple points.
        
        :param operation_callback:
            Function which is called in operations, rebalancing and rotations. Function name
            is the name of the function where the callback is called. 

            \*\*kwargs includes possible info:
                'formula': Formula in which operations are performed
                'target_id': target node id for operations
                'node': target node, of operations if it still/already exists, 
                        bottom node of rebalance algorithm for rebalancing callback
                        top node of rotation for rotation callback.
                'new_label': new label argument for insert/relabel operations
                'insert_left: insert_left argument for insert_leaf operations

            Callback point specifies the location within the function where the callback is called.
            
            Possible callback points are:

            'pre_rotation', 'post_rotation':
                Called before/after each rotation.

            'pre_operation', 'post_local_operation', 'post_operation':
                Called before operation/after local operation/after operation and subsequent rebalance
        :type operation_callback: function(function_name, pointlist, \*\*kwargs)


        :example: 
        A callback function which writes a graph after every rotation:
        
        .. code-block:: python
        
            def operation_callback(func_name, pointlist, **kwargs):
                if 'post_rotation' in pointlist and 'self' in kwargs:
                    kwargs['formula']._to_dot_graph()
                
        """
        self._operation_callback = operation_callback

    def to_dot_graph(self, filename):
        """ Generates a file in which the graph of the formula is painted.

        :param str filename:
            String which represents the name of generated file.
        """
        roots = self._get_roots()
        if not roots:
            dot = Graph(filename)
            dot.render()
        else:
            graph = Graph(filename, filename, engine='dot')
            for root in roots:
                root._to_dot_graph(graph)
                
            graph.render()

    def _get_roots(self):
        """ Finds all roots of the formula by doing a linear search of all nodes.

        :return list:
            All roots in the formula.
        """
        return [node for node in self._nodes.values() if node._parent is None]

    def _bottom_up_pass_all(self, func):
        """ Does a full bottom up pass calling func on each formula leaf and formula inner node. 

        Order can be random but the function is guaranteed to be
        called on both children of each node before being called on the
        node. Raises an InconsistentFormulaError when a circle is found or when
        unreachable nodes were detected.

        Intended mostly for stuff like invariant checking. Modifying the
        formula structure during execution can have unexpected results.

        :param function(node, left_result=None, right_result=None) func:
            Function which takes a single _Node as positional
            argument, the results of the executions of the left
            child/right child as keyword arguments and may return
            results for the parent to work with.
        """

        results = dict() # mapping node id -> returned value
        done = set() # nodes on which the function was called
        left_ready = set() # all nodes where the left child is done
        right_ready = set() # all nodes where the right child is done
        # all nodes where both children are done, initialized with all leaves.
        ready = [self._leaves[leaf_id] for leaf_id in self._leaves]
        while True:
            ready_next_pass = list()
            for node in ready:
                if node in done:
                    raise InconsistentFormulaError(f"Found circle: _Node '{node.id}' encountered twice during bottom up pass.")
                # get values for children
                if(isinstance(node, _InnerNode) and node._left_child is not None):
                    left_result = results[node._left_child.id]
                else:
                    left_result = None
                if(isinstance(node, _InnerNode) and node._right_child is not None):
                    right_result = results[node._right_child.id]
                else:
                    right_result = None
                # execute
                results[node.id] = func(node, left_result=left_result, right_result=right_result)
                done.add(node)
                # handle case node is root
                if node._parent is None:
                    continue
                # mark child node as done for parent
                if node._parent._left_child == node:
                    left_ready.add(node._parent)
                    if node._parent in right_ready: #parent is ready now
                        ready_next_pass.append(node._parent)
                elif node._parent._right_child == node:
                    right_ready.add(node._parent)
                    if node._parent in left_ready: #parent is ready now
                        ready_next_pass.append(node._parent)
                else:
                    raise InconsistentFormulaError(f"Node '{node.id}' is not the child of its parent")
            ready = ready_next_pass
            if not ready:
                break

        #Check if all nodes were encountered
        not_encountered = list()
        for node in self._nodes.values():
            if node not in done:
                not_encountered.append(node)
        if len(not_encountered) > 0:
            raise InconsistentFormulaError(f"Found unreachable nodes:{not_encountered}")
        
    def _simple_relable(self, new_label, target_id):
        """ Relables a tree node.
        
        :param str new_label:
            New label of the node.
        :param str target_id:
            ID for the node on which the label should be changed.
        """
        if target_id not in self._leaves:
            raise InvalidTargetIdError(f"Invalid relabel operation: target node {target_id} not found in formula.")
        node = self._leaves[target_id]
        node._relabel(new_label)
        if self._nfta is not None:
            node._update_signatures(self._nfta._leaf_signature_mapping)    
        
    def relable(self, new_label, target_id):
        """ Relables a tree node.
        
        :param str new_label:
            New label of the node.
        :param str target_id:
            ID for the node on which the label should be changed.
        """
        self._operation_callback('relable', ['pre_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)
        self._simple_relable(new_label, target_id)
        self._operation_callback('relable', ['post_local_operation','post_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)

        if self._nfta is None:
            return
        # Relabel was successfull, _nfta has been set and the new label differs from the old label
        if  ((node_after_relabel is not None)
            and (node_after_relabel._label != node_before_relabel._label)):
            while(node._parent is not None):
                node = node._parent
                node._update_signatures()
               
    def _simple_insert_leaf(self, new_label, target_id=None, new_id=None, insert_left=False):
        """ Inserts a new tree leaf next to another tree node.

        This might break balance factors and depth calculation further
        up the formula and might require a bottom up pass to restore
        consitency.
        
        :param str new_label:
            Label for the new node.
        :param str target_id:
            The new node will be inserted next to the node with this id. None if a root should be added.
        :param str new_id:
            ID for the new tree node.
        :param bool insert_left:
            True if the new node should be inserted left of the targe tree node, False if it should 
            be inserted right of the target tree node.
        """
        # handle case first node is inserted
        if target_id is None or target_id == "None":
            new_leaf = _Leaf(self, new_label, id=new_id)
            if self._nfta is not None:
                new_leaf._update_signatures(self._nfta._leaf_signature_mapping)        
            return
        
        # replace target node (leaf) with an inner node with target node and new node as children
        if target_id not in self._leaves:
            raise InvalidTargetIdError(f"Invalid insert operation: target node {target_id} not found in formula.")
        target_node = self._leaves[target_id]
        target_node_parent = target_node._parent
        target_node_is_right_child = target_node_parent is not None \
                                     and target_node_parent._right_child == target_node
        new_leaf = _Leaf(self, new_label, id=new_id)
        new_inner_node = _InnerNode(self, operation="oplus")

        if self._nfta is not None:
            new_leaf._update_signatures(self._nfta._leaf_signature_mapping)   

        # order of switches needs to be bottom up to correctly calculate context
        # new inner node gets leaf and target node as children
        if insert_left:
            new_inner_node._change_children(new_leaf, target_node)
        else:
            new_inner_node._change_children(target_node, new_leaf)

        # parent of target node gets new inner node as child.
        if target_node_parent is not None:
            if target_node_is_right_child:
                target_node_parent._change_children(target_node_parent._left_child, new_inner_node)
            else:
                target_node_parent._change_children(new_inner_node, target_node_parent._right_child) 
        
    def insert_leaf(self, new_label, target_id=None, new_id=None, insert_left=False):
        """ Inserts a new tree leaf next to another tree node.

        Performs a simple insert and afterwards rebalances and restores formula consistency.
        
        :param str new_label:
            Label for the new node.
        :param str target_id:
            The new node will be inserted next to the node with this id. None if a new root should be added.
        :param str new_id:
            ID for the new tree node.
        :param bool insert_left:
            True if the new node should be inserted left of the target tree node, False if it should 
            be inserted right of the target tree node.
        """
        self._operation_callback('insert_leaf', ['pre_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)
        self._simple_insert_leaf(new_label, target_id, new_id=new_id, insert_left=insert_left)
        self._operation_callback('insert_leaf', ['post_local_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)
        self._bottom_up_rebalance(target_id)
        self._operation_callback('insert_leaf', ['post_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)
        
    def _simple_insert_subdivision(self, new_label, target_id, new_id=None):
        """ Inserts a new tree node below target_node, all children of target tree node now become childern of the newtree node.

        This might break balance factors and depth calculation further
        up the formula and might require a bottom up pass to restore
        consitency.
        
        :param str new_label:
            Label for the new tree node.
        :param str target_id:
            The new tree node will be inserted next to the tree node with this id.
        :param str new_id:
            ID for the new tree node.
        """
        if target_id not in self._leaves:
            raise InvalidTargetIdError(f"Invalid insert_subdivision operation: target node {target_id} not found in formula.")
        target_node = self._leaves[target_id]
        target_node_parent = target_node._parent
        target_node_is_right_child = target_node_parent is not None \
                                     and target_node_parent._right_child == target_node


        # Create new nodes
        new_leaf = _Leaf(self, new_label, id=new_id)
        if self._nfta is not None:
            new_leaf._update_signatures(self._nfta._leaf_signature_mapping) 

        new_inner_node = _InnerNode(self, operation="odot")

        # if target node is already a context the new node will become one too.
        if target_node._is_context:
            new_leaf._is_context = True
        else:
            target_node._is_context = True
            if self._nfta is not None:
                target_node._update_signatures(self._nfta._leaf_signature_mapping)

        # new inner node gets leaf and target node as children
        new_inner_node._change_children(target_node, new_leaf)
        
        # parent of target node gets new inner node as child.
        if target_node_parent is not None:
            if target_node_is_right_child:
                target_node_parent._change_children(target_node_parent._left_child, new_inner_node)
            else:
                target_node_parent._change_children(new_inner_node, target_node_parent._right_child)

    def insert_subdivision(self, new_label, target_id, new_id=None):
        """ Inserts a new tree node below target_node, all children of target tree node now become childern of the newtree node.

        Performs a simple insert and afterwards rebalances and restores formula consistency.
        
        :param str new_label:
            Label for the new tree node.
        :param str target_id:
            The new tree node will be inserted next to the tree node with this id.
        :param str new_id:
            ID for the new tree node.
        """
        self._operation_callback('insert_subdivision', ['pre_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)
        self._simple_insert_subdivision(new_label, target_id, new_id)
        self._operation_callback('insert_subdivision', ['post_local_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)
        self._bottom_up_rebalance(target_id)
        self._operation_callback('insert_subdivision', ['post_operation'], formula=self,
                                 target_id=target_id, new_label=new_label)

    def _simple_delete_leaf(self, target_id):
        """ Deletes a tree leaf.

        Performs a simple delete and restores consistency below the parent of the target node. 
        Might break balance and requires further consistency fixes above the target node.

        :param str target_id:
            Id of the tree leaf that should be deleted.

        :return _Node:
            Node to start bottom up pass in delete_leaf to restore consistency.
        """
        if target_id not in self._leaves:
            raise InvalidTargetIdError(f"""Invalid delete operation: 
                                target leaf {target_id} not found in formula.""")
        target_node = self._leaves[target_id]
        # Case leaf is a context and can not be deleted
        if target_node._is_context:
            raise InvalidTargetIdError(f"""Invalid delete operation: 
                                target leaf {target_id} is a context.""")
        # Case root is deleted
        if target_node._parent is None:
            target_node._delete()
            return None

        target_node_parent = target_node._parent
        target_node_parent_parent = target_node._parent._parent
        target_node_sibling = target_node._getSibling()

        node_for_bottom_up_rebalance = target_node_sibling
        
        # Bottom up pass to find first odot node of which target_node is part of the right subtree 
        have_to_switch_context = True
        current_node = target_node
        while(True): # TODO: ersten if block ersetzen durch assert current_node._parent is not None und brauchen wir überhaupt einen bottom up parse? vllt nicht, da Z. 272 nie ausgeführt wird :D
            if current_node._parent is None:
                assert(isinstance(current_node, _InnerNode))
                if current_node._operation == "oplus":
                    have_to_switch_context = False
                else:
                    raise RuntimeError(f"""Try to delete last child, 
                                        but could not find the original parent.""")
            elif current_node._parent._operation =="oplus":
                have_to_switch_context = False
                break
            elif current_node._parent._operation =="odot":
                if current_node._parent._right_child == current_node:
                    current_node = current_node._parent
                    break
            current_node = current_node._parent

        if have_to_switch_context==True:
            if current_node._left_child._is_context == False:
                 raise WrongContextError(f"""Invalid operand '{target_node_parent.id}':
                                        First operand of odot has to be a context.""")
            # Top down pass to find the parent of target_node
            inner_odot_node = current_node
            current_node = current_node._left_child
            while isinstance(current_node, _InnerNode):
                if current_node._operation == "oplus":
                    if  (
                            current_node._left_child._is_context==True 
                            and current_node._right_child._is_context==True
                        ):
                        raise WrongContextError(f"""Invalid operand '{current_node.id}':
                                                    Cannot oplus two contexts.""")
                    if  (
                            current_node._left_child._is_context==False 
                            and current_node._right_child._is_context==False
                        ):
                        raise WrongContextError(f"""Invalid operand '{current_node.id}':
                                                    One of the children has to be a context.""")
                    if current_node._left_child._is_context==True:
                        current_node = current_node._left_child
                    elif current_node._right_child._is_context==True:
                        current_node = current_node._right_child
                elif current_node._operation == "odot":
                    if current_node._right_child._is_context == True:
                        current_node = current_node._right_child
                    else:
                        raise WrongContextError(f"""Invalid operand '{current_node._right_child.id}':
                                                    Has to be a context.""")
            # current_node is leaf
            assert(isinstance(current_node, _Leaf))
            current_node._is_context = False
            if self._nfta is not None:
                current_node._update_signatures(self._nfta._leaf_signature_mapping) 
            # bottom up parse to fix _is_context above current_node until we reach inner_odot_node
            while(current_node._parent is not None and current_node._parent is not inner_odot_node):
                current_node = current_node._parent
                current_node._update_is_context()
                if self._nfta is not None:
                    current_node._update_signatures() 

            # replace target nodes parent with target nodes sibling in tree.
            if target_node_parent._parent is not None:
                if target_node_parent_parent._left_child == target_node_parent:
                    target_node_parent_parent._change_children(target_node_sibling, target_node_parent_parent._right_child)
                else:
                    target_node_parent_parent._change_children(target_node_parent_parent._left_child, target_node_sibling) 
                target_node_parent_parent._update_is_context()

        else:
            # replace target nodes parent with target nodes sibling in tree.
            if target_node_parent._parent is not None:
                if target_node_parent_parent._left_child == target_node_parent:
                    target_node_parent_parent._change_children(target_node_sibling, target_node_parent_parent._right_child)
                else:
                    target_node_parent_parent._change_children(target_node_parent_parent._left_child, target_node_sibling) 

        # remove target node and target nodes parent
        target_node._delete()
        target_node_parent._delete()
        return node_for_bottom_up_rebalance

    def delete_leaf(self, target_id):
        """ Deletes a tree leaf.

        Performs a simple delete and restores consistency and balance in the formula afterwards.

        :param str target_id:
            Id of the tree leaf that should be deleted.
        """
        self._operation_callback('delete_leaf', ['pre_operation'], formula=self,
                                 target_id=target_id)
        new_parent_of_target = self._simple_delete_leaf(target_id)
        self._operation_callback('delete_leaf', ['post_local_operation'], formula=self,
                                 target_id=target_id)
        if new_parent_of_target is None:
            return
        else:
            self._bottom_up_rebalance(new_parent_of_target.id)
        self._operation_callback('delete_leaf', ['post_operation'], formula=self,
                                 target_id=target_id)

    def _bottom_up_rebalance(self, target_id):
        """ Perform a rebalancing run from the target node to the root.
        
        Updates the signatures afterwards.

        :param str target_id:
            target_id of the node where the rebalancing run should start.
        """
        if target_id is None or target_id == "None":
            return
        
        if target_id not in self._nodes:
            raise InvalidTargetIdError(f"Invalid rebalance operation: target leaf {target_id} not found in formula.")
        target_node = self._nodes[target_id]

        if self._rebalance_algorithm == 'bottom_up_rebalance_once_sibling_niewerth':
            self._bottom_up_rebalance_once_sibling_niewerth(target_node)
        else:
            raise RuntimeError(f'rebalancing algorithm "{self._rebalance_algorithm}" not found')

    def _bottom_up_rebalance_once_sibling_niewerth(self, node):
        """ Rebalancing algorithm proposed by niewerth 2018.

        Does a single rotation at a node and at its sibling. Updates the signatures of all 
        involved children if a nfta has been set.

        :param _Node node:
            Node where the rebalancing run should start.
        """
        passed_nodes = set()
        while (True):
            node._update_balance()
            node._update_is_context()
            rotate(node)
            rotate(node._getSibling())
            if node._parent is None:
                break
            else:
                passed_nodes.add(node)
                if node._parent in passed_nodes:
                    raise InconsistentFormulaError(f"Circle detected during node rebalancing: {node.parent.id}")
                node = node._parent

    def check_consistency(self):
        """ Checks the formula for multiple things and raises InconsistentFormulaError if something is wrong.

        Performs a bottom up pass from all formula leaves upward and checks consistency.
        Checks:

        - If all Nodes context classification is up to date. (WrongContextError)
        - If all Nodes operations are valid for the operands. (WrongOperationError)
        - If all Nodes balance factors are up to date. (WrongBalanceError)
        - If all Nodes are reachable from leaves. (InconsistentFormulaError)
        - If no circles exist in the tree. (InconsistentFormulaError)
        """
        def checker(node, left_result=None, right_result=None):
            node._update_balance(only_check=True)
            node._update_is_context(only_check=True)
            if self._nfta is not None:
                node._update_signatures(self._nfta._leaf_signature_mapping, only_check=True)
        self._bottom_up_pass_all(checker)

    def run_operation_sequence(self, sequence):
        """ Runs a sequence of operations.
        
        :param list sequence:
            List of tuples. ((operation, target[, new_label,  new_id[, insert_left]]),...) Where 
                operation is "ins_leaf", "ins_sub", "delete", 
                target is the target id for the 3 operations, 
                new_label is the label for the new nodes (inserts only),
                new_id is the id for the new node (insert only), 
                insert_left toggles wether insert_leaf is inserted left or right of the target leaf (insert leaf only).
        """
        for op in sequence:
            if len(op) < 2:
                raise RuntimeError(f"invalid operation in sequence: {op}")
            if op[0] == "delete":
                self.delete_leaf(str(op[1]))
            elif op[0] == "ins_sub":
                self.insert_subdivision(str(op[2]), target_id=str(op[1]), new_id=str(op[3]))
            elif op[0] == "ins_leaf":
                if len(op) < 5:
                    insert_left = None
                else:
                    insert_left = op[4]
                self.insert_leaf(str(op[2]), target_id=str(op[1]), new_id=str(op[3]), insert_left=insert_left)

    def to_tree(self, forestenv):
        """ Returns the tree this formula represents.

        Creates leaves and combines them using a bottom-up pass using odot and oplus operations.

        :param ForestEnvironment forestenv: 
            Environment where the tree should be created.

        :return TreeNode:
            Created tree.
        """
        def tree_combiner(node, left_result=None, right_result=None):
            if isinstance(node, _Leaf):
                tree_node = TreeNode(forestenv, node._label, id=node.id)
                if node._is_context:
                    tree_node._to_context_leaf()
                return tree_node
            else:
                if node._operation == 'oplus':
                    return left_result._oplus(right_result)

                elif node._operation == 'odot':
                    return left_result._odot(right_result)

        result = self._bottom_up_pass_all(tree_combiner)
        # set forest envs id counter to reflect ids which were generated for the formula
        forestenv._node_id_counter = self._leaf_id_counter
        return result

    def _update_all_signatures(self):
        """ Bottom up computes the signatures of the tree from the signatures of the leaf nodes.
        """
        if self._nfta is None:
            raise RuntimeError(f"No nfta has been set to formula.")
        leaf_signature_mapping = self._nfta._to_leaf_signature_mapping()
        def updater(node, left_result=None, right_result=None):
            node._update_signatures(leaf_signature_mapping)
        self._bottom_up_pass_all(updater)
