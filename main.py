#!/usr/bin/python3
import argparse
from run_simulations import _generate_experiments, _run_experiments
from interact import interactive
import global_params

def main():
    parser = argparse.ArgumentParser(description='Proof of concept for working with forest algebra formulas')
    parser.add_argument('command', type=str,
                        choices=['generate_simulations','run_simulations','interactive'],
                        default='interactive',
                        help="""Command to perform
                             generate_simulations: generates the sequences required for running the simulations into the ./sequences directory
                             run_simulations: runs simulations on all sequences in the ./sequences directory and writes plots.
                             interactive (default): provides prompt for running sequences.""")
    parser.add_argument('--verbose-trees', '-v', action='store_true',
                        help='Include all fields, not just label and id for printed trees and formulas.')
    parser.add_argument('--all-pdfs', '-p', action='store_true', help='Write pdf before and after each simulation.')
    args = parser.parse_args()
    global_params.ARG_COMMAND = args.command
    global_params.ARG_VERBOSE_TREES = args.verbose_trees
    global_params.ARG_ALL_PDFS = args.all_pdfs
    if global_params.ARG_COMMAND == 'generate_simulations':
        _generate_experiments()
    elif global_params.ARG_COMMAND == 'run_simulations':
        _run_experiments()        
        #_run_simulation()
    elif global_params.ARG_COMMAND == 'interactive':
        interactive()

if __name__ == '__main__':
    main()
