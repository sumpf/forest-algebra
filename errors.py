3#!/usr/bin/python3
class InvalidTargetIdError(RuntimeError):
    """ An Error for when a given target_id is not found in the formula or is not accessible.

    :param str message: 
        Message for the error.
    """
    def __init__(self, message):
        super().__init__(message)

class DuplicateIdError(RuntimeError):
    """ An Error for when a new ID already exists in the formula.

    :param str message: 
        Message for the error.
    """
    def __init__(self, message):
        super().__init__(message)
        
class InconsistentFormulaError(RuntimeError):
    """ An Error for when a circle is found in the formula or when unreachable formula nodes were detected including multiple roots.

    :param str message: 
        Message for the error.
    """
    def __init__(self, message):
        super().__init__(message)

class WrongContextError(InconsistentFormulaError):        
    """ An Error for when the context classification of a formula node is incorrect.

    :param str message: 
        Message for the error.
    """
    def __init__(self, message):
        super().__init__(message)

class WrongOperationError(InconsistentFormulaError):
    """ An Error for invalid operation combinations:

        - context oplus context,
        - forest odot context, 
        - forest odot forest.

    :param str message: 
        Message for the error.
    """
    def __init__(self, message):
        super().__init__(message)

class WrongBalanceError(InconsistentFormulaError):
    """ An Error for when the balance factor of a formula node is incorrect.

    :param str message: 
        Message for the error.
    """
    def __init__(self, message):
        super().__init__(message)

class WrongAutomatonError(RuntimeError):
    """ An error for when an invalid/incomplete set of signatures is provided by the user. """
    def __init__(self, message):
        super().__init__(message)
