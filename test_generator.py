#!/usr/bin/python3
from generator import SequenceGenerator
from tree_node import ForestEnvironment
from formula import Formula

def test_generate_random_sequence_probability():
    #check generate only ins_leaf
    generator = SequenceGenerator()
    sequence = generator.generate_random_sequence(length=100, del_prob=0, ins_leaf_prob=1)
    assert [] == [op for op in sequence if op[0] in ['delete','ins_sub']]
    forest_env = ForestEnvironment()
    forest_env.run_operation_sequence(sequence)

    #check generate only ins_sub, one ins_leaf for root needs to be included
    generator = SequenceGenerator()
    sequence = generator.generate_random_sequence(length=100, del_prob=0, ins_leaf_prob=0)
    assert sequence[0][0] == 'ins_leaf'
    assert [] == [op for op in sequence[1:] if op[0] in ['delete','ins_leaf']]
    forest_env = ForestEnvironment()
    forest_env.run_operation_sequence(sequence)
    
    #check generate delete only, every other operation should be ins_leaf
    generator = SequenceGenerator()
    sequence = generator.generate_random_sequence(length=100, del_prob=1, ins_leaf_prob=0)
    assert [] == [op for op in sequence if op[0] == 'ins_sub']
    assert [] == [op for op in sequence[0::2] if op[0] in ['delete','ins_sub']]
    assert [] == [op for op in sequence[1::2] if op[0] in ['ins_sub','ins_leaf']]
    forest_env = ForestEnvironment()
    forest_env.run_operation_sequence(sequence)

def test_generate_random_sequence_forest_env_state():
    #check if a the provided forest environment is correctly copied and ensures generation of valid sequences
    forest_env = ForestEnvironment()
    initial_sequence = [("ins_leaf", None, "0", "0"),
                        ("ins_leaf", "0", "1", "1"),
                        ("ins_leaf", "0", "2", "2", True),
                        ("ins_leaf", "0", "3", "3", False),
                        ("ins_sub", "0", "4","4"),
                        ("ins_sub", "4", "5","5"),
                        ("ins_sub", "5", "6","6"),
                        ("ins_sub", "6", "7","7"),
                        ("ins_sub", "7", "8","8"),
                        ("ins_sub", "7", "9","9"),
                        ("ins_sub", "7", "10","10"),
                        ("ins_sub", "7", "11","11"),
                        ("ins_sub", "7", "12","12"),
                        ("ins_sub", "7", "13","13")]
    #generate a forest
    forest_env.run_operation_sequence(initial_sequence)
    assert forest_env.count_nodes() == len(initial_sequence)
    
    generator = SequenceGenerator(forest_env=forest_env)
    seq_0 = generator.generate_random_sequence(length=8, del_prob=1)
    assert [] == [op for op in seq_0[1::2] if op[0] in ['ins_sub','ins_leaf']]
    
    # generators copy of forest env should have lost 8 nodes by now, while forest_env should remain the same
    assert generator.forest_env.count_nodes() == len(initial_sequence) - 8
    assert forest_env.count_nodes() == len(initial_sequence)
    seq_1 = generator.generate_random_sequence(len(initial_sequence) - 8, del_prob=1)
    
    # generators copy of forest env should be empty now
    assert generator.forest_env.count_nodes() == 0
    
    # running the sequences on the forest environment should work and result in en empty forest environment
    forest_env.run_operation_sequence(seq_0 + seq_1)
    assert forest_env.count_nodes() == 0
    
def test_k_ary_tree_sequence():
    for k in range(1, 10):
        length = 150
        length -= length % k - 1
        generator = SequenceGenerator()
        op_sequence = generator.generate_random_k_ary_tree_sequence(k=k, length=length)

        assert [] == [op for op in op_sequence if op[0] == 'delete']
        
        forest_env = ForestEnvironment()
        # check length and consistency on tree_nodes
        forest_env.run_operation_sequence(op_sequence)
        forest_env._check_consistency()
        assert forest_env.count_nodes() == length

        # check consistency on formula
        formula = Formula()
        formula.run_operation_sequence(op_sequence)
        formula.check_consistency()
        forest_env_2 = ForestEnvironment()
        formula.to_tree(forest_env_2)
        assert forest_env_2.get_roots()[0].forest_equals(forest_env.get_roots()[0])

        # check k-aryness. Each node has either 0 or k children
        for node in forest_env._nodes.values():
            assert len(node.children) in [0, k]

def test_balanced_k_ary_sequence():
    for k in range(1, 10):
        length = 150
        length -= length % k - 1
        generator = SequenceGenerator()
        op_sequence = generator.generate_balanced_k_ary_tree_sequence(k=k, length=length)

        assert [] == [op for op in op_sequence if op[0] == 'delete']
        
        forest_env = ForestEnvironment()
        # check length and consistency on tree_nodes
        forest_env.run_operation_sequence(op_sequence)
        forest_env._check_consistency()
        assert forest_env.count_nodes() == length

        # check consistency on formula
        formula = Formula()
        formula.run_operation_sequence(op_sequence)
        formula.check_consistency()
        forest_env_2 = ForestEnvironment()
        formula.to_tree(forest_env_2)
        assert forest_env_2.get_roots()[0].forest_equals(forest_env.get_roots()[0])

        # check k-aryness. Each node has either 0 or k children
        for node in forest_env._nodes.values():
            assert len(node.children) in [0, k]

        # check balance
        all_heights = forest_env.get_height(get_all_heights=True)
        for node in forest_env._nodes.values():
            if node.children:
                max_height = max([all_heights[child.id] for child in node.children])
                assert all([max_height - all_heights[child.id] <= 1
                            for child in node.children])
    

def test_generate_bounded_height_tree():
    for height in range(1,10):
        length = 200
        generator = SequenceGenerator()
        op_sequence = generator.generate_bounded_height_tree(height=height, length=length, ins_leaf_prob=0.4)

        assert [] == [op for op in op_sequence if op[0] == 'delete']

        forest_env = ForestEnvironment()
        # check length, height and consistency on tree nodes
        forest_env.run_operation_sequence(op_sequence)
        forest_env._check_consistency()
        assert forest_env.get_height() <= height

        # check consistency on formula
        formula = Formula()
        formula.run_operation_sequence(op_sequence)
        formula.check_consistency()
        forest_env_2 = ForestEnvironment()
        formula.to_tree(forest_env_2)
        assert forest_env_2.get_roots()[0].forest_equals(forest_env.get_roots()[0])
