#!/usr/bin/python3
from setuptools import setup

try:
    from sphinx.setup_command import BuildDoc
except:
    def BuildDoc():
        raise RuntimeError('Sphinx is needed in order to build the documentation')

cmdclass = {'build_sphinx': BuildDoc}
docs_require=['sphinx','sphinxcontrib-apidoc','sphinxcontrib-serializinghtml']

setup(name='forest_algebra',
      version='1.0',
      description='Proof of concept for using forest algebra formulars as a data structure.',
      author='Jonas Marasus, Sarah Kleest-Meißner',
      py_modules=['forest_algebra'],
      setup_requires=['pytest-runner'],
      install_requires=[
          'numpy',
          'pandas',
          'graphviz',
          'msgpack',
          'matplotlib',
          'sphinx',
          'sphinx_rtd_theme'],
      tests_require=['pytest'],
      cmdclass={'build_sphinx':BuildDoc},
      command_options={
          'build_sphinx': {
              'config_dir':('setup.py','doc'),
              'source_dir':('setup.py','doc/source'),
              'builder':('setup.py','html'),
              'build_dir':('setup.py','doc/build')}})
