#!/usr/bin/python3
from run_simulations import _write_sequence, _read_sequence, _collect_statistics, _plot_statistics, _generate_experiments, _run_experiment, _run_experiments
from tree_node import *
from formula import Formula

def test_generate_experiment():
    experiments = _generate_experiments()

def test_run_experiment_1():
    stats_for_experiment = _run_experiment("random.0.lin.300.0.seq")
    for stats in stats_for_experiment:
        print(stats)

def test_run_experiment_2():
    stats_for_experiment = _run_experiment("const-height-k.1.lin.100.0.seq")
    for stats in stats_for_experiment:
        print(stats)

def test_plots():
    #tree_type_list_small = ["random", "random-k-ary", "const-height-k"]
    #parameters_small = {'random-k-ary':[3], 'balanced-k-ary':[3], 'const-height-k':[3]}
    #tree_sizes_small = {'lin':range(100, 500, 50),
    #                    'log':[int(pow(2, exponent/2)) for exponent in range(12, 21)]}

    #_generate_experiments(tree_type_list=None, parameters=None, tree_sizes=None)
    stats_for_experiment = _run_experiments()
    _plot_statistics(stats_for_experiment)

def test_collect_statistic():
    op_sequence_without_first = [('ins_leaf', '0', 2, 2, False), ('ins_sub', '0', 3, 3), ('delete', '2'), ('ins_sub', '3', 5, 5), ('ins_sub', '5', 6, 6), ('ins_leaf', '3', 7, 7, True), ('ins_leaf', '6', 8, 8, True), ('ins_leaf', '6', 10, 10, False), ('ins_sub', '3', 12, 12), ('ins_leaf', '7', 13, 13, True), ('delete', '10'), ('ins_sub', '13', 15, 15), ('ins_leaf', '6', 16, 16, True), ('ins_leaf', '8', 17, 17, True), ('ins_sub', '16', 18, 18), ('ins_leaf', '8', 19, 19, True), ('ins_leaf', '17', 20, 20, False), ('ins_leaf', '12', 23, 23, True), ('ins_sub', '5', 24, 24), ('ins_leaf', '18', 26, 26, True), ('ins_sub', '13', 27, 27), ('ins_sub', '7', 29, 29)]

    formula = Formula()
    formula.run_operation_sequence([('ins_leaf', None, 0, 0, False)])
    stats = _collect_statistics(formula, op_sequence_without_first)

    formula.to_dot_graph("TEST-FOR-STATS")
    forest_env = ForestEnvironment()
    tree = formula.to_tree(forest_env)
    forest_env.to_dot_graph("TEST-FOR-STATS-TREE")
