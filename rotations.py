3#!/usr/bin/python3
from node import _InnerNode, _Leaf
"""Contains methods for the rotations"""

def rotate(node):
    """Applies a single rotation to a node.

    The first rotation within applicable within the rotation order will be applied.

    :param _Node node: 
        _Node to be treated as "v" for the rotation. No nodes above this node are affected.
    
    :return bool:
        True if a rotation was performed, False if no rotation was possible.
    """
    for rotation in rotation_order:
        if rotation(node): return True
    return False

def rotation1a(node):
    # Check node structure
    if not isinstance(node, _InnerNode):
        return False
    # Check balance factors
    if not (node._balance_factor>=2 and node._right_child._balance_factor>=0):
        return False
    # Check operations
    if not ((node._operation=="odot" and node._right_child._operation=="odot")
            or(node._operation=="oplus" and node._right_child._operation=="oplus")):
        return False

    node._formula._operation_callback('rotation1a', ['pre_rotation'], formula=node._formula, node=node)
                                       
    w = node._right_child
    tree_1 = node._left_child
    tree_2 = w._left_child
    tree_3 = w._right_child
    w._change_children(tree_1,tree_2)
    node._change_children(w,tree_3)
                                       
    node._formula._operation_callback('rotation1a', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation1b(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor <= -2 
            and node._left_child._balance_factor <= 0):
        return False
    #Check operations
    if not ((node._operation=="odot" and node._left_child._operation=="odot") 
            or (node._operation=="oplus" and node._left_child._operation=="oplus")):
        return False

    node._formula._operation_callback('rotation1b', ['pre_rotation'], formula=node._formula, node=node)
    w = node._left_child
    tree_1 = w._left_child
    tree_2 = w._right_child
    tree_3 = node._right_child
    w._change_children(tree_2,tree_3)
    node._change_children(tree_1,w)
    node._formula._operation_callback('rotation1b', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation1bspecial(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not node._balance_factor <= -2:
        return False
    #Check operations
    if not (node._operation == "odot" and node._left_child._operation=="odot"):
        return False

    node._formula._operation_callback('rotation1bspecial', ['pre_rotation'], formula=node._formula, node=node)
    w = node._left_child
    tree_1 = w._left_child
    tree_2 = w._right_child
    tree_3 = node._right_child
    w._change_children(tree_2,tree_3)
    node._change_children(tree_1,w)
    node._formula._operation_callback('rotation1bspecial', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation2a(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor <= -2 and node._left_child._balance_factor > 0):
        return False
    #balance factor implies left child must be inner node
    assert isinstance(node._left_child, _InnerNode)
    #Check operations
    if not ((
                node._operation == "odot"
                and node._left_child._operation == "odot"
                and node._left_child._right_child._operation == "odot"
            ) or (
                node._operation == "oplus"
                and node._left_child._operation == "oplus"
                and node._left_child._right_child._operation == "oplus")
            ):
        return False

    node._formula._operation_callback('rotation2a', ['pre_rotation'], formula=node._formula, node=node)
    w = node._left_child
    u = node._left_child._right_child
    tree_1 = w._left_child
    tree_2 = u._left_child
    tree_3 = u._right_child
    tree_4 = node._right_child
    w._change_children(tree_1,tree_2)
    u._change_children(tree_3,tree_4)
    node._change_children(w,u)
    node._formula._operation_callback('rotation2a', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation2b(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor >= 2
            and node._right_child._balance_factor < 0):
        return False
    #balance factor implies right child must be inner node
    assert isinstance(node._right_child, _InnerNode)
    #Check operations
    if not ((
                node._operation == "odot"
                and node._right_child._operation == "odot"
                and node._right_child._left_child._operation == "odot"
            ) or (
                node._operation == "oplus"
                and node._right_child._operation == "oplus"
                and node._right_child._left_child._operation == "oplus")
            ):
        return False

    node._formula._operation_callback('rotation2b', ['pre_rotation'], formula=node._formula, node=node)
    w = node._right_child
    u = node._right_child._left_child
    tree_1 = node._left_child
    tree_2 = u._left_child
    tree_3 = u._right_child
    tree_4 = w._right_child
    w._change_children(tree_1,tree_2)
    u._change_children(tree_3,tree_4)
    node._change_children(w,u)
    node._formula._operation_callback('rotation2b', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation3a(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor <= -2
            and node._left_child._balance_factor >= 0):
        return False
    #Check operations
    if not (node._operation == "oplus" 
            and node._left_child._operation == "odot"):
        return False
    #Check extra condition
    if not (node._right_child._is_context == False):
        return False

    node._formula._operation_callback('rotation3a', ['pre_rotation'], formula=node._formula, node=node)
    w = node._left_child
    tree_1 = w._left_child
    tree_2 = node._right_child
    tree_3 = w._right_child
    w._operation = "oplus"
    node._operation="odot"
    w._change_children(tree_1,tree_2)    
    node._change_children(w,tree_3)
    node._formula._operation_callback('rotation3a', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation3b(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor <= -2):
        return False
    #Check operations
    if not (node._operation == "odot" 
            and node._left_child._operation == "oplus"):
        return False
    #Check extra condition
    if not (node._left_child._left_child._is_context == True):
        return False

    node._formula._operation_callback('rotation3b', ['pre_rotation'], formula=node._formula, node=node)
    w = node._left_child
    tree_1 = w._left_child
    tree_2 = w._right_child
    tree_3 = node._right_child
    w._operation = "odot"
    node._operation = "oplus"
    w._change_children(tree_1,tree_3)
    node._change_children(w,tree_2)
    node._formula._operation_callback('rotation3b', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation3c(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor <= -2):
        return False
    #Check opeartions
    if not (node._left_child._operation == "oplus" and node._operation == "odot"):
        return False
    #Check extra condition
    if not (node._left_child._right_child._is_context == True):
        return False

    node._formula._operation_callback('rotation3c', ['pre_rotation'], formula=node._formula, node=node)
    w = node._left_child
    tree_1 = w._left_child
    tree_2 = w._right_child
    tree_3 = node._right_child
    w._operation = "odot"
    node._operation = "oplus"
    w._change_children(tree_2,tree_3)
    node._change_children(tree_1,w)
    node._formula._operation_callback('rotation3c', ['post_rotation'], formula=node._formula, node=node)
    return True

def rotation3d(node):
    #Check node structure
    if not isinstance(node, _InnerNode):
        return False
    #Check balance factors
    if not (node._balance_factor >= 2 
            and node._right_child._balance_factor >= 0):
        return False
    #Check operations
    if not (node._operation == "oplus"
            and node._right_child._operation == "odot"):
        return False
    #Check extra condition
    if not(node._left_child._is_context == False):
        return False

    node._formula._operation_callback('rotation3d', ['pre_rotation'], formula=node._formula, node=node)
    w = node._right_child
    tree_1 = node._left_child
    tree_2 = w._left_child
    tree_3 = w._right_child
    w._operation = "oplus"
    node._operation="odot"
    w._change_children(tree_1,tree_2)
    node._change_children(w,tree_3)
    node._formula._operation_callback('rotation3d', ['post_rotation'], formula=node._formula, node=node)
    return True

rotation_order=(rotation1a, rotation2a, rotation1b, rotation1bspecial, rotation2b, rotation3a, rotation3b, rotation3c, rotation3d)
