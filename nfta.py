#!/usr/bin/python3

class NFTA():
    def __init__(self, alphabet, states, init, transitions):
        """ Class for representing nftas.

        Calculates a mapping for each symbol to the signatures a leaf
        with that symbol should get:
            {(symbol, is_context):{((q_1,q_2)[,(q_3, q5)]),...}
        Signature has 2 components if the leaf node represents a forest 
        and 2x2 components if the leaf represents a context.
        
        :param list alphabet:
            List of the nfta's symbols.
        :param list states:
            List of the nfta's states.
        :param dict init:
            Mapping from symbol to list of initial states.
        :param dict transitions:
            Mapping of 2-tuple of states to state.
        """
        self._alphabet = alphabet
        self._states = states
        self._init = init
        self._transitions = transitions

        # signatures for context symbols
        leaf_signature_mapping = dict()
        for symbol in self._alphabet:
            if symbol not in self._init:
                raise WrongAutomatonError(f'Wrong nfta: no init for symbol {symbol}')
            for q1 in self._init[symbol]:
                for (q2, q3) in self._transitions:
                    # context
                    for q4 in self._transitions[(q2,q3)]:
                        if (symbol, True) not in leaf_signature_mapping:
                            leaf_signature_mapping[(symbol, True)] = set()
                        leaf_signature_mapping[(symbol, True)].add(((q2,q4),(q1,q3)))
                    # non-context
                    if q1 == q3:
                        for q4 in self._transitions[(q2,q3)]:
                            if (symbol, False) not in leaf_signature_mapping:
                                leaf_signature_mapping[(symbol, False)] = set()
                            leaf_signature_mapping[(symbol, False)].add((q2,q4))

        self._leaf_signature_mapping = leaf_signature_mapping

