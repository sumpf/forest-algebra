#!/usr/bin/python3
from tree_node import ForestEnvironment, TreeNode
from errors import *
from copy import deepcopy

def test_little_tree():
    env = ForestEnvironment()
    node_a = TreeNode(env, "a")
    node_b = TreeNode(env, "b")
    node_c = TreeNode(env, "c")

    node_a._insert_subdivision(node_b)
    node_b._insert_leaf(node_c, False)
    node_c._relabel("cutie")
    node_c._delete()

    env.to_dot_graph("test_tree_node")

def test_delete_all():
    env = ForestEnvironment()
    node_a = TreeNode(env, "a")
    node_b = TreeNode(env, "b")
    node_c = TreeNode(env, "c")

    node_a._insert_subdivision(node_b)
    node_b._insert_leaf(node_c, False)

    node_c._delete()
    node_b._delete()
    node_a._delete()
    env.to_dot_graph("test_tree_node")

def test_multiple_roots():
    env = ForestEnvironment()
    node_a = TreeNode(env, "a")
    node_b = TreeNode(env, "b")
    node_c = TreeNode(env, "c")

    node_a1 = TreeNode(env, "a1")
    node_a2 = TreeNode(env, "a2")
    node_a3 = TreeNode(env, "a3")
    node_a4 = TreeNode(env, "a4")
    node_a5 = TreeNode(env, "a5")
    node_b1 = TreeNode(env, "b1")
    
    node_a._insert_subdivision(node_a1)
    node_a1._insert_leaf(node_a2, True)
    node_a1._insert_leaf(node_a3, False)
    node_a2._insert_leaf(node_a4, True)
    node_a2._insert_leaf(node_a5, False)

    #following line fails: correct RuntimeError
    #node_a._insert_leaf(node_a2, False)

    node_b._insert_subdivision(node_b1)

    node_c._delete()
    node_b1._delete()
    node_b._delete()    
    node_a1._delete()
    node_a5._delete()

    env.to_dot_graph("test_tree_node")

def test_oplus():
    env = ForestEnvironment()
    node_a = TreeNode(env, "a")

    node_a1 = TreeNode(env, "a1")
    node_a2 = TreeNode(env, "a2")
    node_a3 = TreeNode(env, "a3")
    node_a4 = TreeNode(env, "a4")
    node_a5 = TreeNode(env, "a5")
    
    node_a._insert_subdivision(node_a1)
    node_a1._insert_leaf(node_a2, True)
    node_a1._insert_leaf(node_a3, False)
    node_a2._insert_leaf(node_a4, True)
    node_a2._insert_leaf(node_a5, False)

    node_c = TreeNode(env, "c")

    node_a._oplus(node_c)

    env.to_dot_graph("test_tree_node")

def test_forest_equal():
    op_sequence = [('ins_leaf', None, 0, 0, False),
                   ('ins_leaf', '0', 2, 2, False),
                   ('ins_sub', '2', 3, 3),
                   ('ins_sub', '0', 5, 5),
                   ('ins_sub', '0', 6, 6),
                   ('ins_leaf', '2', 7, 7, True),
                   ('ins_leaf', '0', 8, 8, True),
                   ('ins_leaf', '2', 10, 10, True),
                   ('ins_sub', '6', 11, 11),
                   ('ins_sub', '7', 12, 12),
                   ('ins_sub', '10', 13, 13),
                   ('ins_sub', '8', 15, 15),
                   ('ins_leaf', '0', 16, 16, True),
                   ('ins_leaf', '12', 17, 17, True),
                   ('ins_sub', '0', 18, 18),
                   ('ins_leaf', '10', 19, 19, True),
                   ('ins_leaf', '6', 20, 20, False),
                   ('ins_leaf', '16', 23, 23, True),
                   ('ins_sub', '2', 24, 24),
                   ('ins_leaf', '24', 26, 26, False),
                   ('ins_leaf', '12', 27, 27, True),
                   ('ins_sub', '10', 28, 28),
                   ('ins_sub', '12', 29, 29)]
    env = ForestEnvironment()
    env.run_operation_sequence(op_sequence)
    env_2 = ForestEnvironment()
    env_2.run_operation_sequence(op_sequence)
    # same sequence needs to create equal forest
    assert env.get_roots()[0].forest_equals(env_2.get_roots()[0])

    # any modified sequence needs to create unequal forest as there are no nops.
    for op in op_sequence[1:]:
        modified_sequence = op_sequence.copy()
        modified_sequence.remove(op)
        env_2 = ForestEnvironment()
        for op2 in modified_sequence:
            try:
                env_2.run_operation_sequence([op2])
            except InvalidTargetIdError as error:
                continue
        assert not env.get_roots()[0].forest_equals(env_2.get_roots()[0])

def test_copy_forest_env():
    op_sequence = [('ins_leaf', None, 0, 0, False),
                   ('ins_leaf', '0', 2, 2, False),
                   ('ins_sub', '2', 3, 3),
                   ('ins_sub', '0', 5, 5),
                   ('ins_sub', '0', 6, 6),
                   ('ins_leaf', '2', 7, 7, True),
                   ('ins_leaf', '0', 8, 8, True),
                   ('ins_leaf', '2', 10, 10, True),
                   ('ins_sub', '6', 11, 11),
                   ('ins_sub', '7', 12, 12),
                   ('ins_sub', '10', 13, 13),
                   ('ins_sub', '8', 15, 15),
                   ('ins_leaf', '0', 16, 16, True),
                   ('ins_leaf', '12', 17, 17, True),
                   ('ins_sub', '0', 18, 18),
                   ('ins_leaf', '10', 19, 19, True),
                   ('ins_leaf', '6', 20, 20, False),
                   ('ins_leaf', '16', 23, 23, True),
                   ('ins_sub', '2', 24, 24),
                   ('ins_leaf', '24', 26, 26, False),
                   ('ins_leaf', '12', 27, 27, True),
                   ('ins_sub', '10', 28, 28),
                   ('ins_sub', '12', 29, 29)]
    env = ForestEnvironment()
    env.run_operation_sequence(op_sequence)
    env_copy = deepcopy(env)

    env.to_dot_graph("ENV")
    env.to_dot_graph("ENV_COPY")

    assert env.get_roots()[0].forest_equals(env_copy.get_roots()[0])
