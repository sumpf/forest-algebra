#!/usr/bin/python3
# includes default values for arguments which may or may not be changed by the main function
global ARG_COMMAND
ARG_COMMAND = 'test'

global ARG_VERBOSE_TREES
ARG_VERBOSE_TREES = False

global ARG_ALL_PDFS
ARG_ALL_PDFS = False
