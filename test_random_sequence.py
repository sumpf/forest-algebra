#!/usr/bin/python3
from rotations import rotate
import node
from formula import Formula
from tree_node import ForestEnvironment
from functools import partial
from collections import defaultdict
from errors import *

import random

def test_random_insert_sequence():

    formula = Formula()
    formula.insert_leaf("0", None, new_id="0")
    nodes = {"0"}
    leaves = {"0"}
    
    random.seed(0)
    nodecount = 500

    op_sequence = list()
    op_sequence.append(('ins_leaf', None, 0, 0, False))
    try:
        for label in range(1, nodecount):
            label = str(label)
            # add type:
            if (random.random()<0.4): # add leaf
                # insert next to random leaf (randomly left/right)
                op = ("ins_leaf", random.sample(leaves, 1)[0], label, label, random.random() > 0.5)
                op_sequence.append(op)
                formula.run_operation_sequence(list([op]))
                leaves.add(str(label))
                nodes.add(str(label))
            else:
                # insert above random node
                op = ("ins_sub", random.sample(nodes, 1)[0], label, label)
                op_sequence.append(op)
                formula.run_operation_sequence(list([op]))
                nodes.add(str(label))
                
            formula.check_consistency()
    except Exception as error:
        with open("FAIL_test_random_insert_sequence.txt", 'w') as sequence_output:
            sequence_output.write(str(op_sequence))
        formula.to_dot_graph("FAIL_test_random_insert_sequence" + str(label))
        raise error

def test_random_sequence_():

    formula = Formula()
    formula.insert_leaf("0", None, new_id="0")
    nodes = {"0"}

    random.seed(0)
    nodecount = 30
    op_sequence = list()
    op_sequence.append(('ins_leaf', None, 0, 0, False))
    try:
        for label in range(1, nodecount):
            # add type:
            case_number = random.random()
            if (case_number<0.4):
                # insert next to random leaf (randomly left/right)
                op = ("ins_leaf", random.sample(nodes, 1)[0], label, label, random.random() > 0.5)
                op_sequence.append(op)
                formula.run_operation_sequence(list([op]))
                nodes.add(str(label))
            elif (case_number<0.8):
                # insert above random node
                op = ("ins_sub", random.sample(nodes, 1)[0], label, label)
                op_sequence.append(op)
                formula.run_operation_sequence(list([op]))
                nodes.add(str(label))
            else:
                # delete a leaf
                try:
                    target_node_id = random.sample(nodes, 1)[0]
                    if(target_node_id != "0"):
                        if target_node_id in formula._leaves:
                            target_node = formula._leaves[target_node_id]
                            if target_node._is_context==False:
                                print('try delete_leaf {}'.format(target_node_id))  
                                op = ("delete", target_node_id)
                                op_sequence.append(op)
                                formula.run_operation_sequence(list([op]))    
                                nodes.remove(str(target_node_id))
                            else:
                                pass
                                #print('node {} is leaf but context, can not be deleted'.format(target_node_id))
                        else:
                            pass
                            #print('node {} is not a leaf, can not be deleted'.format(target_node_id))
                    else:
                        pass
                        #print('target_node_is is 0 and will be skipped.')
                except InvalidTargetIdError as e:
                    #if e.args[0].find("not found in formula"):
                        #print('InvalidTargetIdError: node {} not found'.format(target_node_id))
                    #if e.args[0].find("is a context"):
                        #print('InvalidTargetIdError: node {} is context'.format(target_node_id))
                    #if e.args[0].find("is a context"):
                        #print('InvalidTargetIdError: Invalid rebalance operation at node {}'.format(target_node_id))
                    pass
            formula.check_consistency()
    except Exception as error:
        with open("FAIL_test_random_sequence.txt", 'w') as sequence_output:
            sequence_output.write(str(op_sequence))
        formula.to_dot_graph("FAIL_test_random_sequence" + str(label))
        raise error
    # check tree handling
    forest_env = ForestEnvironment()
    try:
        for op in op_sequence:
            forest_env.run_operation_sequence([op])
            forest_env._check_consistency()
            
    except Exception as error:
        forest_env.to_dot_graph("FAIL_test_random_sequence_tree" + str(label))
        raise error

    # check creation of tree from formula
    try:
        forest_env_2 = ForestEnvironment()
        formula.to_tree(forest_env_2)
        assert forest_env_2.get_roots()[0].forest_equals(forest_env.get_roots()[0])
    except Exception as error:
        forest_env_2.to_dot_graph('FAIL_test_random_sequence_to_tree_forest')
        formula.to_dot_graph('FAIL_test_random_sequence_to_tree_formula')
        forest_env.to_dot_graph("FAIL_test_random_sequence_tree")
        raise error

    with open("FAIL_test_random_sequence.txt", 'w') as sequence_output:
        sequence_output.write(str(op_sequence))

    # check operation callback
    formula_2 = Formula()
    pointcounts = defaultdict(int)
    def operation_callback_unbound(func_name, pointlist, pointcounts=None, **kwargs):
        # count occurences of points
        for point in ['pre_rotation', 'post_rotation', 'pre_operation', 'post_local_operation', 'post_operation']:
            if point in pointlist:
                pointcounts[point] += 1

        # pre and post rotation need to have the same count after each operation
        if 'post_rotation' in pointlist:
            assert pointcounts['pre_rotation'] == pointcounts['post_rotation']

        # pre, post_Local and post operation need to have the same count after each operation
        if 'post_operation' in pointlist:
            assert pointcounts['pre_operation'] == pointcounts['post_local_operation']
            assert pointcounts['post_local_operation'] == pointcounts['post_operation']

    operation_callback = partial(operation_callback_unbound, pointcounts=pointcounts)
    formula_2.set_operation_callback(operation_callback)
    formula_2.run_operation_sequence(op_sequence)
    
    assert pointcounts['pre_rotation'] == pointcounts['post_rotation']
    assert pointcounts['pre_operation'] == pointcounts['post_local_operation']
    assert pointcounts['post_local_operation'] == pointcounts['post_operation']
    assert pointcounts['post_rotation'] > 0
    assert pointcounts['post_operation'] > 0
    
        
def test_random_sequence_delete_all_nodes():

    formula = Formula()
    formula.insert_leaf("0", None, new_id="0")
    nodes = {"0"}

    random.seed(0)
    nodecount = 10
    try:
        for label in range(1, nodecount):
            # add type:
            case_number = random.random()
            if (case_number<0.4):
                print('insert_leaf')
                # insert next to random leaf (randomly left/right)
                formula.insert_leaf(str(label), random.sample(nodes,1)[0],
                                    new_id=str(label), insert_left=random.random() > 0.5)
                nodes.add(str(label))
            else:
                print('subdivision')
                # insert above random node
                formula.insert_subdivision(str(label), random.sample(nodes, 1)[0], new_id=str(label))
                nodes.add(str(label))
            formula.check_consistency()

        while len(nodes)>1:
            target_node_id = random.sample(nodes, 1)[0]
            if(target_node_id != "0"):
                if target_node_id in formula._leaves:
                    target_node = formula._leaves[target_node_id]
                    if target_node._is_context==False:
                        print('try delete_leaf {}'.format(target_node_id))           
                        formula.delete_leaf(target_node_id)
                        nodes.remove(str(target_node_id))
                    else:
                        print('node {} is leaf but context, can not be deleted'.format(target_node_id))
                else:
                    print('node {} is not a leaf, can not be deleted'.format(target_node_id))
            else:
                print('target_node_is is 0 and will be skipped.')
        print('Delete node 0')
        formula.delete_leaf("0")
    except Exception as error:
        formula.to_dot_graph("FAIL_test_random_sequence_delete_all_nodes" + str(label))
        raise error

