#!/usr/bin/python3
from node import _InnerNode, _Leaf
from tree_node import *
from formula import Formula
from generator import SequenceGenerator
import global_params
import msgpack
import os
from collections import defaultdict
from functools import partial
import re
from copy import deepcopy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time

FILENAME_REGEXP = '^(?P<tree_type>[^\\.]*)\\.(?P<param>[0-9]*)\\.(?P<tree_scale>[^\\.]*)\\.(?P<tree_size>[0-9]*)\\.(?P<iteration>[0-9]*)\\.seq'
COLORS = ['black', 'red', 'coral', 'peru', 'lawngreen', 'forestgreen', 'lightseagreen', 'teal', 'aqua', 'steelblue', 'mediumblue', 'mediumpurple', 'purple', 'fuchsia', 'mediumvioletred', 'crimson', 'brown']

def _write_sequence(op_sequence, filename):
    """ Packs a sequence of operations as described in function run_operation_sequence in a binary file.

    :param list op_sequence: 
        Sequence to be stored.
    :param str filename: 
        A string which stores the name of the binary file the user wants to create.
    """
    if len(op_sequence)==0:
        raise RuntimeError(f"no op_sequence given")

    if not os.path.exists('sequences'):
        os.mkdir('sequences')
    
    with open('sequences/'+filename+'.msgpack', 'wb') as outfile:
        msgpack.pack(op_sequence, outfile, use_bin_type=True)

def _read_sequence(filename):
    """ Loads and unpacks a binary file.

    :param str filename: 
        A string which stores the name of the file the user wants to unpack.

    :return list:
        Sequence of operations as described in function run_operation_sequence which is stored in file.
    """
    if filename is None:
        raise RuntimeError(f"no name of file given")
    with open('sequences/'+filename+'.msgpack', 'rb') as read_file:
        op_sequence = msgpack.unpack(read_file, raw=False)
    return op_sequence

def _run_simulation():
    stats_list = _run_experiments()
    _plot_statistics(stats_list)

def _generate_experiments(tree_type_list=None, parameters=None, tree_sizes=None):
    """ Generates and writes sequences of different tree types and tree sizes. 

        Creates "done.txt" in the end. Creates a *.seq.mpack and a
        *.done.txt file for each experiment. Parameters can be
        extracted from the filename using FILENAME_REGEXP.

    :param list tree_type_list:
        Types of trees we currently have.

    :param list parameters:
        Special parameters for some tree types.

    :param list tree_sizes:
        Possible sizes of trees.
    """
    if not os.path.exists('sequences'):
        os.mkdir('sequences')

    if tree_type_list is None: 
        tree_type_list = ["random", 
                          "random_flat",
                          "random_deep", 
                          "random-k-ary",
                          "balanced-k-ary",
                          "const-height-k"
                        ]

    if parameters is None:
        parameters = {'random-k-ary':[2, 3, 10, 100],
                      'balanced-k-ary':[2, 3, 10, 100],
                      'const-height-k':[1, 3, 5]
                     }
    parameters = defaultdict(lambda :[0], parameters) # default parameter is 0

    # number of sequences which should be generated for each tree
    iterations = range(0,1)
    
    if tree_sizes is None:
        tree_sizes = {'lin':range(100, 1000, 50),# linear tree size list
                    # log tree size list. starting with 64 (2^(12/2)),
                    # ending with 2048 (2^(256/2)) relative steps of sqrt(2)
                    'log':[int(pow(2, exponent/2)) for exponent in range(12, 21)]}

    # cross product loop for all possible combinations
    for tree_size, tree_type, tree_scale, param, i in [(tree_size, tree_type, tree_scale, param, i)
                                                       for tree_type in tree_type_list
                                                       #for tree_scale in ['lin']
                                                       for tree_scale in ['lin', 'log']
                                                       for tree_size in tree_sizes[tree_scale]
                                                       for param in parameters[tree_type]
                                                       for i in iterations]:
        filename_base = f"{tree_type}.{str(param)}.{tree_scale}.{str(tree_size)}.{str(i)}"
        filename = filename_base + ".seq"
        filename_done = filename_base + ".done.txt"

        if os.path.isfile(filename_base + ".done.txt"):
            continue
        
        generator = SequenceGenerator()
        if tree_type == "random":
            op_sequence = generator.generate_random_sequence(length=tree_size)
        elif tree_type == "random_deep":
            op_sequence = generator.generate_random_sequence(length=tree_size, ins_leaf_prob=0.4)
        elif tree_type == "random_flat":
            op_sequence = generator.generate_random_sequence(length=tree_size, ins_leaf_prob=0.6)
        elif tree_type == "random-k-ary":
            adapted_tree_size = tree_size- (tree_size % param -1)
            op_sequence = generator.generate_random_k_ary_tree_sequence(k=param, length=adapted_tree_size)
        elif tree_type == "balanced-k-ary":
            adapted_tree_size = tree_size- (tree_size % param -1)
            op_sequence = generator.generate_balanced_k_ary_tree_sequence(k=param, length=adapted_tree_size)
        elif tree_type == "const-height-k":
            op_sequence = generator.generate_bounded_height_tree(height=param, length=tree_size, ins_leaf_prob=0.5)
        else:
            raise RuntimeError(f"Unknown tree type:{tree_type}")

        # check if filename is parsable
        parsed_params = re.match(FILENAME_REGEXP,filename)
        assert parsed_params.group('tree_type') == tree_type
        assert int(parsed_params.group('param')) == param
        assert parsed_params.group('tree_scale') == tree_scale
        assert int(parsed_params.group('tree_size')) == tree_size
        assert int(parsed_params.group('iteration')) == i
        _write_sequence(op_sequence, filename)
        open('sequences/' + filename_done, 'a').close()
    
    f = open('sequences/done.txt', 'w')
    f.write("Generate experiment done!")
    f.close()

def _run_experiment(filename):
    """ Performs multiple random operation sequences based on the same given formula. Collects stats
    using function _collect_statistics.

    :param str filename:
        Name of file the user wants to read. This file contains the operation sequence which leads
        to the starting formula.

    :return list:
        List of stats for each random operation sequence.
    """
    
    stats_list = []
    combinations =  [(checked_op_sequence_length, checked_op_sequence_del_prob, checked_op_sequence_ins_leaf_prob)
                        for checked_op_sequence_length in [1, 50]
                        for checked_op_sequence_del_prob in [0, 1]
                        for checked_op_sequence_ins_leaf_prob in [0, 1]
                        if not checked_op_sequence_del_prob + checked_op_sequence_ins_leaf_prob == 2
                    ]

    formula = Formula()
    forest_env = ForestEnvironment()
    op_sequence = _read_sequence(filename)
    formula.run_operation_sequence(op_sequence)
    formula.to_tree(forest_env)

    #parse parameters:
    parsed_params = re.match(FILENAME_REGEXP,filename)
    tree_type = parsed_params.group('tree_type')
    tree_param = int(parsed_params.group('param'))
    tree_scale = parsed_params.group('tree_scale')
    tree_size = int(parsed_params.group('tree_size'))
    tree_iteration = int(parsed_params.group('iteration'))

    for (checked_op_sequence_length, checked_op_sequence_del_prob, checked_op_sequence_ins_leaf_prob) in combinations:
        formula_copy = deepcopy(formula)
        forest_env_copy = deepcopy(forest_env)
        generator = SequenceGenerator(forest_env_copy)
        checked_op_sequence = generator.generate_random_sequence(length = checked_op_sequence_length, del_prob = checked_op_sequence_del_prob, ins_leaf_prob = checked_op_sequence_ins_leaf_prob)

        if global_params.ARG_ALL_PDFS:
            formula_copy.to_dot_graph(filename+"_"+str(checked_op_sequence_length)+"-"+str(checked_op_sequence_del_prob)+"-"+str(checked_op_sequence_ins_leaf_prob)+"_"+"_BEFORE_op_seq_FORMULA")

            forest_env_copy.to_dot_graph(filename+"_"+str(checked_op_sequence_length)+"-"+str(checked_op_sequence_del_prob)+"-"+str(checked_op_sequence_ins_leaf_prob)+"_"+"_BEFORE_op_seq_TREE")

        stats = _collect_statistics(formula_copy, checked_op_sequence)

        if global_params.ARG_ALL_PDFS:
            formula_copy.to_dot_graph(filename+"_"+str(checked_op_sequence_length)+"-"+str(checked_op_sequence_del_prob)+"-"+str(checked_op_sequence_ins_leaf_prob)+"_"+"_AFTER_op_seq_FORMULA")

            forest_env_copy.to_dot_graph(filename+"_"+str(checked_op_sequence_length)+"-"+str(checked_op_sequence_del_prob)+"-"+str(checked_op_sequence_ins_leaf_prob)+"_"+"_AFTER_op_seq_TREE")

        stats["param tree_type"] = tree_type
        stats["param tree_size"] = tree_size
        stats["param tree_param"] = tree_param
        stats["param tree_scale"] = tree_scale
        stats["param tree_iteration"] = tree_iteration
        stats["param seq length"] = checked_op_sequence_length
        stats["param seq del_prob"] = checked_op_sequence_del_prob
        stats["param seq ins_leaf_prob"] = checked_op_sequence_ins_leaf_prob
        stats_list.append(stats)

    return stats_list

def _run_experiments():
    """Performs multiple random operation sequences based on the formulas saved in the directory '/sequences'. 
    Collects stats using function _collect_statistics.

    :return list:
        List of stats for each generated sequence and each random operation sequence.
    """
    stats_list = []
    entries = os.listdir("sequences/")
    for entry in entries:
        if entry.endswith(".msgpack"):
            entry_filename = entry[0:len(entry)-8]
            stats_list.extend(_run_experiment(entry_filename))

    return stats_list
    
def _calc_balance_parameters(formula):
    """ Calculates balance parameters of the given formula.

    :param Formula formula: 
        Formula of which the user wants to get balance parameters.

    :return tuple:
        A tuple in which the first entry represents the number of unbalanced nodes and the second
        entry represents the maximum balance factor of the formula. 
    """
    unbalanced_nodecount = 0
    max_balance_factor = 0
    for node in formula._nodes.values():
        if node._balance_factor < -2 or node._balance_factor > 2:
            unbalanced_nodecount = unbalanced_nodecount+1
        if abs(node._balance_factor) > max_balance_factor:
            max_balance_factor = abs(node._balance_factor)
    return(unbalanced_nodecount, max_balance_factor)

def _operation_callback_unbound(func_name, pointlist, rot_counts=None, rot_dict=None, **kwargs):
    """ Defines a callback function to count rotations.
    """
    # count occurences of points
    if 'post_rotation' in pointlist:
        rot_counts['post_rotation'] += 1
        rot_dict[func_name] += 1

def _collect_statistics(formula, op_sequence):
    """ Collects statistics after running the op_sequence, which is described in function run_operation_sequence,
        and creates a dict in which the entries represents
        - treeheight
        - formulaheight
        - number of nodes in tree
        - number of nodes in formula
        - number of rotations during the op_sequence
        - number of unbalanced nodes in formula
        - max { |B(v) | v node in formula }
        - number of calls of each executed rotation as a dict.
    
    Explicitly intended for statistics of one update on the given formula.
    
    :param Formula formula: 
        Actual formula to which a sequence of operations wil    is_1 =  stats_list_dataframe['param seq length']==1
    filtered_dataframe = stats_list_dataframe[is_1]

    is_prob_0 = filtered_dataframe[]==0
    is_ins_prob_1 =
    is_del_prob_1 =l be applied.
    :param list op_sequence:
        Sequence of operations the user wants statistics about.
    :return dict:
        Dict of the form described above.
    """
    if len(formula._get_roots() ) == 0:
        raise RuntimeError(f"no roots in formula")
    if len(op_sequence)==0:
        raise RuntimeError(f"no op_sequqnce given")

    rot_dict = defaultdict(int)
    rot_counts = defaultdict(int)
    operation_callback = partial(_operation_callback_unbound, rot_counts=rot_counts, rot_dict=rot_dict)
    formula.set_operation_callback(operation_callback)

    #run op_sequence
    start_time = time.time()
    formula.run_operation_sequence(op_sequence)
    runtime = time.time()-start_time

    #create dedicated tree and collect statistic after the sequence of operations
    forest_env = ForestEnvironment()
    tree = formula.to_tree(forest_env)
    tree_height = forest_env.get_height()
    formula_height = max([max(root._left_depth, root._right_depth)+1 for root in formula._get_roots()] + [0])
    nodecount_tree = forest_env.count_nodes()
    nodecount_formula = len(formula._nodes)
    rotationcount = rot_counts['post_rotation']
    unbalanced_nodecount, max_balance_factor = _calc_balance_parameters(formula)

    statistics_after_update = {
        'treeheight': tree_height, 
        'formulaheight': formula_height, 
        'nodecount tree': nodecount_tree, 
        'nodecount formula': nodecount_formula,
        '#rotations': rotationcount,
        '#unbal nodes': unbalanced_nodecount,
        'max bf': max_balance_factor,
        'runtime': runtime}

    statistics_after_update.update(rot_dict)
    return statistics_after_update

def _plot_statistics(stats_list):
    """Plots a given statistic.

    :param list stats_list: 
        List of dicts of the form described in function _collect_statistic.
    """

    stats_list_dataframe = pd.DataFrame([stats_list[0]])
    for stats in stats_list:
        stats_df = pd.DataFrame([stats])
        stats_list_dataframe = stats_list_dataframe.append(stats_df)
 
    _plot_nodecount_tree_to_formulaheight_opseq1(stats_list_dataframe)
    _plot_nodecount_tree_to_formulaheight_opseq50(stats_list_dataframe)
    #TODO: doesn't work the right way
    #_plot_nodecount_tree_to_formulaheight_max_mean_median_values(stats_list_dataframe)

    _plot_nodecount_tree_to_rotations_opseq1(stats_list_dataframe)
    _plot_nodecount_tree_to_rotations_opseq50(stats_list_dataframe)

    _plot_nodecount_tree_to_unbalanced_nodes(stats_list_dataframe)
    _plot_nodecount_tree_to_max_balancefactor(stats_list_dataframe)

    _plot_nodecount_tree_to_runtime_opseq50(stats_list_dataframe)
    _plot_nodecount_tree_to_runtime_opseq1(stats_list_dataframe)

    stats_list_dataframe.to_csv('plots/dataframe')

def _plot_nodecount_tree_to_formulaheight_opseq1(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the height of the associated formula.
    Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """
    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')
    is_1 =  stats_list_dataframe['param seq length']==1
    filtered_dataframe = stats_list_dataframe[is_1]

    is_lin = filtered_dataframe['param tree_scale']=='lin'
    is_log = filtered_dataframe['param tree_scale']=='log'

    filtered_dataframe_lin = filtered_dataframe[is_lin]
    filtered_dataframe_log = filtered_dataframe[is_log]

    lin_balanced_k_ary = filtered_dataframe_lin['param tree_type']=='balanced-k-ary'
    dataframe_lin_bal_k_ary = filtered_dataframe_lin[lin_balanced_k_ary]
    lin_const_height_k = filtered_dataframe_lin['param tree_type']=='const-height-k'
    dataframe_lin_const_height_k = filtered_dataframe_lin[lin_const_height_k]
    lin_random_k_ary = filtered_dataframe_lin['param tree_type']=='random-k-ary'
    dataframe_lin_random_k_ary = filtered_dataframe_lin[lin_random_k_ary]
    lin_random = filtered_dataframe_lin['param tree_type']=='random'
    dataframe_lin_random = filtered_dataframe_lin[lin_random]
    lin_random_flat = filtered_dataframe_lin['param tree_type']=='random_flat'
    dataframe_lin_random_flat = filtered_dataframe_lin[lin_random_flat]
    lin_random_deep = filtered_dataframe_lin['param tree_type']=='random_deep'
    dataframe_lin_random_deep = filtered_dataframe_lin[lin_random_deep]

    log_balanced_k_ary = filtered_dataframe_log['param tree_type']=='balanced-k-ary'
    dataframe_log_bal_k_ary = filtered_dataframe_log[log_balanced_k_ary]
    log_const_height_k = filtered_dataframe_log['param tree_type']=='const-height-k'
    dataframe_log_const_height_k = filtered_dataframe_log[log_const_height_k]
    log_random_k_ary = filtered_dataframe_log['param tree_type']=='random-k-ary'
    dataframe_log_random_k_ary = filtered_dataframe_log[log_random_k_ary]
    log_random = filtered_dataframe_log['param tree_type']=='random'
    dataframe_log_random = filtered_dataframe_log[log_random]
    log_random_flat = filtered_dataframe_log['param tree_type']=='random_flat'
    dataframe_log_random_flat = filtered_dataframe_log[log_random_flat]
    log_random_deep = filtered_dataframe_log['param tree_type']=='random_deep'
    dataframe_log_random_deep = filtered_dataframe_log[log_random_deep]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_lin.png', format='png')

    colourcount=0

    #lin plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_lin_bal_k_ary.png', format='png')

    #lin plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_lin_random_k_ary.png', format='png')

    #lin plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_lin_const_height_k.png', format='png')

    #lin plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_lin_random-flat-deep.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_log.png', format='png')

    colourcount=0
    #log plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_log_bal_k_ary.png', format='png')

    #log plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_log_random_k_ary.png', format='png')

    #log plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_log_const_height_k.png', format='png')

    #log plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq1_log_random-flat-deep.png', format='png')

def _plot_nodecount_tree_to_formulaheight_opseq50(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the height of the associated formula.
    Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """
    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')
    is_50 =  stats_list_dataframe['param seq length']==50
    filtered_dataframe = stats_list_dataframe[is_50]

    is_lin = filtered_dataframe['param tree_scale']=='lin'
    is_log = filtered_dataframe['param tree_scale']=='log'

    filtered_dataframe_lin = filtered_dataframe[is_lin]
    filtered_dataframe_log = filtered_dataframe[is_log]

    lin_balanced_k_ary = filtered_dataframe_lin['param tree_type']=='balanced-k-ary'
    dataframe_lin_bal_k_ary = filtered_dataframe_lin[lin_balanced_k_ary]
    lin_const_height_k = filtered_dataframe_lin['param tree_type']=='const-height-k'
    dataframe_lin_const_height_k = filtered_dataframe_lin[lin_const_height_k]
    lin_random_k_ary = filtered_dataframe_lin['param tree_type']=='random-k-ary'
    dataframe_lin_random_k_ary = filtered_dataframe_lin[lin_random_k_ary]
    lin_random = filtered_dataframe_lin['param tree_type']=='random'
    dataframe_lin_random = filtered_dataframe_lin[lin_random]
    lin_random_flat = filtered_dataframe_lin['param tree_type']=='random_flat'
    dataframe_lin_random_flat = filtered_dataframe_lin[lin_random_flat]
    lin_random_deep = filtered_dataframe_lin['param tree_type']=='random_deep'
    dataframe_lin_random_deep = filtered_dataframe_lin[lin_random_deep]

    log_balanced_k_ary = filtered_dataframe_log['param tree_type']=='balanced-k-ary'
    dataframe_log_bal_k_ary = filtered_dataframe_log[log_balanced_k_ary]
    log_const_height_k = filtered_dataframe_log['param tree_type']=='const-height-k'
    dataframe_log_const_height_k = filtered_dataframe_log[log_const_height_k]
    log_random_k_ary = filtered_dataframe_log['param tree_type']=='random-k-ary'
    dataframe_log_random_k_ary = filtered_dataframe_log[log_random_k_ary]
    log_random = filtered_dataframe_log['param tree_type']=='random'
    dataframe_log_random = filtered_dataframe_log[log_random]
    log_random_flat = filtered_dataframe_log['param tree_type']=='random_flat'
    dataframe_log_random_flat = filtered_dataframe_log[log_random_flat]
    log_random_deep = filtered_dataframe_log['param tree_type']=='random_deep'
    dataframe_log_random_deep = filtered_dataframe_log[log_random_deep]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_lin.png', format='png')

    colourcount=0
    #lin plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_lin_bal_k_ary.png', format='png')

    #lin plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_lin_random_k_ary.png', format='png')

    #lin plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_lin_const_height_k.png', format='png')

    #lin plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_lin_random-flat-deep.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_log.png', format='png')

    colourcount=0
    #log plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_log_bal_k_ary.png', format='png')

    #log plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_log_random_k_ary.png', format='png')

    #log plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_log_const_height_k.png', format='png')

    #log plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_opseq50_log_random-flat-deep.png', format='png')

def _plot_nodecount_tree_to_formulaheight_max_mean_median_values(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the height of the associated formula.
    Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')
    is_lin = stats_list_dataframe['param tree_scale']=='lin'
    is_log = stats_list_dataframe['param tree_scale']=='log'
    dataframe_lin = stats_list_dataframe[is_lin]
    dataframe_log = stats_list_dataframe[is_log]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    #TODO: groupby on param seq length instead of nodecount. Something strange happens then, checkout!
    stats_list_dataframe_max = stats_list_dataframe.groupby(['param tree_type', 'nodecount tree']).max().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(stats_list_dataframe_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_maxvalues.png', format='png')

    #lin plot
    dataframe_lin_max = dataframe_lin.groupby(['param tree_type', 'nodecount tree']).max().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_maxvalues_lin.png', format='png')

    #log plot
    dataframe_log_max = dataframe_log.groupby(['param tree_type', 'nodecount tree']).max().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_maxvalues_log.png', format='png')

    ################################################################################################
    
    #TODO: groupby on param seq length instead of nodecount
    stats_list_dataframe_max = stats_list_dataframe.groupby(['param tree_type', 'nodecount tree']).mean().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(stats_list_dataframe_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_meanvalues.png', format='png')

    #lin plot
    dataframe_lin_max = dataframe_lin.groupby(['param tree_type', 'nodecount tree']).mean().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_meanvalues_lin.png', format='png')

    #log plot
    dataframe_log_max = dataframe_log.groupby(['param tree_type', 'nodecount tree']).mean().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_meanvalues_log.png', format='png')
    
    ################################################################################################

    #TODO: groupby on param seq length instead of nodecount
    stats_list_dataframe_max = stats_list_dataframe.groupby(['param tree_type', 'nodecount tree']).median().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(stats_list_dataframe_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_medianvalues.png', format='png')

    #lin plot
    dataframe_lin_max = dataframe_lin.groupby(['param tree_type', 'nodecount tree']).median().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_medianvalues_lin.png', format='png')

    #log plot
    dataframe_log_max = dataframe_log.groupby(['param tree_type', 'nodecount tree']).median().reset_index()
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_max.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='formulaheight', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_formulaheight_medianvalues_log.png', format='png')


def _plot_nodecount_tree_to_rotations_opseq1(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the number of rotations during 
    an operation sequence of length 1 in the associated formula. Saves the plot as '.png'.

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')

    is_1 =  stats_list_dataframe['param seq length']==1
    filtered_dataframe = stats_list_dataframe[is_1]

    #is_prob_0 = filtered_dataframe[]==0 #TODO: how? both probs have to be zero!
    is_ins_prob_1 = filtered_dataframe['param seq ins_leaf_prob']==0
    is_del_prob_1 = filtered_dataframe['param seq del_prob']==0

    filtered_dataframe_ins_prob_1 = filtered_dataframe[is_ins_prob_1]
    filtered_dataframe_del_prob_1 = filtered_dataframe[is_del_prob_1]

    is_lin = filtered_dataframe['param tree_scale']=='lin'
    is_log = filtered_dataframe['param tree_scale']=='log'
    is_lin_ins_prob_1 = filtered_dataframe_ins_prob_1['param tree_scale']=='lin'
    is_lin_del_prob_1 = filtered_dataframe_del_prob_1['param tree_scale']=='lin'
    is_log_ins_prob_1 = filtered_dataframe_ins_prob_1['param tree_scale']=='log'
    is_log_del_prob_1 = filtered_dataframe_del_prob_1['param tree_scale']=='log'

    filtered_dataframe_lin = filtered_dataframe[is_lin]
    filtered_dataframe_log = filtered_dataframe[is_log]
    filtered_dataframe_lin_ins_prob_1 = filtered_dataframe_ins_prob_1[is_lin_ins_prob_1]
    filtered_dataframe_lin_del_prob_1 = filtered_dataframe_del_prob_1[is_lin_del_prob_1]
    filtered_dataframe_log_ins_prob_1 = filtered_dataframe_ins_prob_1[is_log_ins_prob_1]
    filtered_dataframe_log_del_prob_1 = filtered_dataframe_del_prob_1[is_log_del_prob_1]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq1_lin.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq1_log.png', format='png')

    #lin plot, ins_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin_ins_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq1_lin_ins_prob_1.png', format='png')

    #lin plot, del_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin_del_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq1_lin_del_prob_1.png', format='png')

    #log plot, ins_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log_ins_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq1_log_ins_prob_1.png', format='png')

    #log plot, del_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log_del_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq1_log_del_prob_1.png', format='png')

def _plot_nodecount_tree_to_rotations_opseq50(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the number of rotations during 
    an operation sequence of length 50 in the associated formula. Saves the plot as '.png'.

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')

    is_50 =  stats_list_dataframe['param seq length']==50
    filtered_dataframe = stats_list_dataframe[is_50]

    #is_prob_0 = filtered_dataframe[]==0 #TODO: how? both probs have to be zero!
    is_ins_prob_1 = filtered_dataframe['param seq ins_leaf_prob']==0
    is_del_prob_1 = filtered_dataframe['param seq del_prob']==0

    filtered_dataframe_ins_prob_1 = filtered_dataframe[is_ins_prob_1]
    filtered_dataframe_del_prob_1 = filtered_dataframe[is_del_prob_1]

    is_lin = filtered_dataframe['param tree_scale']=='lin'
    is_log = filtered_dataframe['param tree_scale']=='log'
    is_lin_ins_prob_1 = filtered_dataframe_ins_prob_1['param tree_scale']=='lin'
    is_lin_del_prob_1 = filtered_dataframe_del_prob_1['param tree_scale']=='lin'
    is_log_ins_prob_1 = filtered_dataframe_ins_prob_1['param tree_scale']=='log'
    is_log_del_prob_1 = filtered_dataframe_del_prob_1['param tree_scale']=='log'

    filtered_dataframe_lin = filtered_dataframe[is_lin]
    filtered_dataframe_log = filtered_dataframe[is_log]
    filtered_dataframe_lin_ins_prob_1 = filtered_dataframe_ins_prob_1[is_lin_ins_prob_1]
    filtered_dataframe_lin_del_prob_1 = filtered_dataframe_del_prob_1[is_lin_del_prob_1]
    filtered_dataframe_log_ins_prob_1 = filtered_dataframe_ins_prob_1[is_log_ins_prob_1]
    filtered_dataframe_log_del_prob_1 = filtered_dataframe_del_prob_1[is_log_del_prob_1]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq50_lin.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq50_log.png', format='png')

    #lin plot, ins_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin_ins_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq50_lin_ins_prob_1.png', format='png')

    #lin plot, del_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin_del_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq50_lin_del_prob_1.png', format='png')

    #log plot, ins_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log_ins_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq50_log_ins_prob_1.png', format='png')

    #log plot, del_prob = 1
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log_del_prob_1.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#rotations', c=COLORS[i], label=key)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_rotations_opseq50_log_del_prob_1.png', format='png')

def _plot_nodecount_tree_to_unbalanced_nodes(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the number of unbalanced nodes in 
    the associated formula. Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """
    #TODO: distinguish between different param seq length

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')
    is_lin = stats_list_dataframe['param tree_scale']=='lin'
    is_log = stats_list_dataframe['param tree_scale']=='log'

    dataframe_lin = stats_list_dataframe[is_lin]
    dataframe_log = stats_list_dataframe[is_log]

    lin_balanced_k_ary = dataframe_lin['param tree_type']=='balanced-k-ary'
    dataframe_lin_bal_k_ary = dataframe_lin[lin_balanced_k_ary]
    lin_const_height_k = dataframe_lin['param tree_type']=='const-height-k'
    dataframe_lin_const_height_k = dataframe_lin[lin_const_height_k]
    lin_random_k_ary = dataframe_lin['param tree_type']=='random-k-ary'
    dataframe_lin_random_k_ary = dataframe_lin[lin_random_k_ary]
    lin_random = dataframe_lin['param tree_type']=='random'
    dataframe_lin_random = dataframe_lin[lin_random]
    lin_random_flat = dataframe_lin['param tree_type']=='random_flat'
    dataframe_lin_random_flat = dataframe_lin[lin_random_flat]
    lin_random_deep = dataframe_lin['param tree_type']=='random_deep'
    dataframe_lin_random_deep = dataframe_lin[lin_random_deep]

    log_balanced_k_ary = dataframe_log['param tree_type']=='balanced-k-ary'
    dataframe_log_bal_k_ary = dataframe_log[log_balanced_k_ary]
    log_const_height_k = dataframe_log['param tree_type']=='const-height-k'
    dataframe_log_const_height_k = dataframe_log[log_const_height_k]
    log_random_k_ary = dataframe_log['param tree_type']=='random-k-ary'
    dataframe_log_random_k_ary = dataframe_log[log_random_k_ary]
    log_random = dataframe_log['param tree_type']=='random'
    dataframe_log_random = dataframe_log[log_random]
    log_random_flat = dataframe_log['param tree_type']=='random_flat'
    dataframe_log_random_flat = dataframe_log[log_random_flat]
    log_random_deep = dataframe_log['param tree_type']=='random_deep'
    dataframe_log_random_deep = dataframe_log[log_random_deep]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(stats_list_dataframe.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[i], label=key)
    plt.ylim(-10,300)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes.png', format='png')
    
    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[i], label=key)
    plt.ylim(-10,300)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_lin.png', format='png')

    colourcount=0
    #lin plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_lin_bal_k_ary.png', format='png')

    #lin plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_lin_random_k_ary.png', format='png')

    #lin plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_lin_const_height_k.png', format='png')

    #lin plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_lin_random-flat-deep.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[i], label=key)
    plt.ylim(-10,300)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_log.png', format='png')

    colourcount=0
    #log plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_log_bal_k_ary.png', format='png')

    #log plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_log_random_k_ary.png', format='png')

    #log plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_log_const_height_k.png', format='png')

    #log plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='#unbal nodes', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-10,300)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_unbalanced_nodes_log_random-flat-deep.png', format='png')

def _plot_nodecount_tree_to_max_balancefactor(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the height of the associated formula.
    Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """
    #TODO: distinguish between different param seq length

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')

    is_lin = stats_list_dataframe['param tree_scale']=='lin'
    is_log = stats_list_dataframe['param tree_scale']=='log'

    dataframe_lin = stats_list_dataframe[is_lin]
    dataframe_log = stats_list_dataframe[is_log]

    lin_balanced_k_ary = dataframe_lin['param tree_type']=='balanced-k-ary'
    dataframe_lin_bal_k_ary = dataframe_lin[lin_balanced_k_ary]
    lin_const_height_k = dataframe_lin['param tree_type']=='const-height-k'
    dataframe_lin_const_height_k = dataframe_lin[lin_const_height_k]
    lin_random_k_ary = dataframe_lin['param tree_type']=='random-k-ary'
    dataframe_lin_random_k_ary = dataframe_lin[lin_random_k_ary]
    lin_random = dataframe_lin['param tree_type']=='random'
    dataframe_lin_random = dataframe_lin[lin_random]
    lin_random_flat = dataframe_lin['param tree_type']=='random_flat'
    dataframe_lin_random_flat = dataframe_lin[lin_random_flat]
    lin_random_deep = dataframe_lin['param tree_type']=='random_deep'
    dataframe_lin_random_deep = dataframe_lin[lin_random_deep]

    log_balanced_k_ary = dataframe_log['param tree_type']=='balanced-k-ary'
    dataframe_log_bal_k_ary = dataframe_log[log_balanced_k_ary]
    log_const_height_k = dataframe_log['param tree_type']=='const-height-k'
    dataframe_log_const_height_k = dataframe_log[log_const_height_k]
    log_random_k_ary = dataframe_log['param tree_type']=='random-k-ary'
    dataframe_log_random_k_ary = dataframe_log[log_random_k_ary]
    log_random = dataframe_log['param tree_type']=='random'
    dataframe_log_random = dataframe_log[log_random]
    log_random_flat = dataframe_log['param tree_type']=='random_flat'
    dataframe_log_random_flat = dataframe_log[log_random_flat]
    log_random_deep = dataframe_log['param tree_type']=='random_deep'
    dataframe_log_random_deep = dataframe_log[log_random_deep]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(stats_list_dataframe.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor.png', format='png')

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_lin.png', format='png')

    colourcount=0
    #lin plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_lin_bal_k_ary.png', format='png')

    #lin plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_lin_random_k_ary.png', format='png')

    #lin plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_lin_const_height_k.png', format='png')

    #lin plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_lin_random-flat-deep.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[i], label=key)
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_log.png', format='png')

    colourcount=0
    #log plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_log_bal_k_ary.png', format='png')

    #log plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_log_random_k_ary.png', format='png')

    #log plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_log_const_height_k.png', format='png')

    #log plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='max bf', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(-5,25)
    plt.xlim(-50,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_max_balancefactor_log_random-flat-deep.png', format='png')

def _plot_nodecount_tree_to_runtime_opseq1(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the required runtime for executing 
    an operation sequence of length 50 on the associated formula.
    Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')
    is_1 =  stats_list_dataframe['param seq length']==1
    filtered_dataframe = stats_list_dataframe[is_1]

    is_lin = filtered_dataframe['param tree_scale']=='lin'
    is_log = filtered_dataframe['param tree_scale']=='log'

    filtered_dataframe_lin = filtered_dataframe[is_lin]
    filtered_dataframe_log = filtered_dataframe[is_log]

    lin_balanced_k_ary = filtered_dataframe_lin['param tree_type']=='balanced-k-ary'
    dataframe_lin_bal_k_ary = filtered_dataframe_lin[lin_balanced_k_ary]
    lin_const_height_k = filtered_dataframe_lin['param tree_type']=='const-height-k'
    dataframe_lin_const_height_k = filtered_dataframe_lin[lin_const_height_k]
    lin_random_k_ary = filtered_dataframe_lin['param tree_type']=='random-k-ary'
    dataframe_lin_random_k_ary = filtered_dataframe_lin[lin_random_k_ary]
    lin_random = filtered_dataframe_lin['param tree_type']=='random'
    dataframe_lin_random = filtered_dataframe_lin[lin_random]
    lin_random_flat = filtered_dataframe_lin['param tree_type']=='random_flat'
    dataframe_lin_random_flat = filtered_dataframe_lin[lin_random_flat]
    lin_random_deep = filtered_dataframe_lin['param tree_type']=='random_deep'
    dataframe_lin_random_deep = filtered_dataframe_lin[lin_random_deep]

    log_balanced_k_ary = filtered_dataframe_log['param tree_type']=='balanced-k-ary'
    dataframe_log_bal_k_ary = filtered_dataframe_log[log_balanced_k_ary]
    log_const_height_k = filtered_dataframe_log['param tree_type']=='const-height-k'
    dataframe_log_const_height_k = filtered_dataframe_log[log_const_height_k]
    log_random_k_ary = filtered_dataframe_log['param tree_type']=='random-k-ary'
    dataframe_log_random_k_ary = filtered_dataframe_log[log_random_k_ary]
    log_random = filtered_dataframe_log['param tree_type']=='random'
    dataframe_log_random = filtered_dataframe_log[log_random]
    log_random_flat = filtered_dataframe_log['param tree_type']=='random_flat'
    dataframe_log_random_flat = filtered_dataframe_log[log_random_flat]
    log_random_deep = filtered_dataframe_log['param tree_type']=='random_deep'
    dataframe_log_random_deep = filtered_dataframe_log[log_random_deep]

    if not os.path.exists('plots'):
        os.mkdir('plots')

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[i], label=key)
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_lin.png', format='png')

    colourcount=0
    #lin plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_lin_bal_k_ary.png', format='png')

    #lin plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_lin_random_k_ary.png', format='png')

    #lin plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_lin_const_height_k.png', format='png')

    #lin plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_lin_random-flat-deep.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[i], label=key)
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_log.png', format='png')

    colourcount=0
    #log plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_log_bal_k_ary.png', format='png')

    #log plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_log_random_k_ary.png', format='png')

    #log plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_log_const_height_k.png', format='png')

    #log plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    #plt.ylim(-0.05,0.06)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq1_log_random-flat-deep.png', format='png')

def _plot_nodecount_tree_to_runtime_opseq50(stats_list_dataframe):
    """ Plots the correlation between the number of nodes in a tree and the required runtime for executing 
    an operation sequence of length 50 on the associated formula.
    Saves the plot as '.png'

    :param DataFrame stats_list_dataframe:
        DataFrame which contains the stats created by the function _run_experiments().
    """

    stats_list_dataframe = stats_list_dataframe.sort_values(by='nodecount tree')
    is_50 =  stats_list_dataframe['param seq length']==50
    filtered_dataframe = stats_list_dataframe[is_50]

    is_lin = filtered_dataframe['param tree_scale']=='lin'
    is_log = filtered_dataframe['param tree_scale']=='log'

    filtered_dataframe_lin = filtered_dataframe[is_lin]
    filtered_dataframe_log = filtered_dataframe[is_log]

    lin_balanced_k_ary = filtered_dataframe_lin['param tree_type']=='balanced-k-ary'
    dataframe_lin_bal_k_ary = filtered_dataframe_lin[lin_balanced_k_ary]
    lin_const_height_k = filtered_dataframe_lin['param tree_type']=='const-height-k'
    dataframe_lin_const_height_k = filtered_dataframe_lin[lin_const_height_k]
    lin_random_k_ary = filtered_dataframe_lin['param tree_type']=='random-k-ary'
    dataframe_lin_random_k_ary = filtered_dataframe_lin[lin_random_k_ary]
    lin_random = filtered_dataframe_lin['param tree_type']=='random'
    dataframe_lin_random = filtered_dataframe_lin[lin_random]
    lin_random_flat = filtered_dataframe_lin['param tree_type']=='random_flat'
    dataframe_lin_random_flat = filtered_dataframe_lin[lin_random_flat]
    lin_random_deep = filtered_dataframe_lin['param tree_type']=='random_deep'
    dataframe_lin_random_deep = filtered_dataframe_lin[lin_random_deep]

    log_balanced_k_ary = filtered_dataframe_log['param tree_type']=='balanced-k-ary'
    dataframe_log_bal_k_ary = filtered_dataframe_log[log_balanced_k_ary]
    log_const_height_k = filtered_dataframe_log['param tree_type']=='const-height-k'
    dataframe_log_const_height_k = filtered_dataframe_log[log_const_height_k]
    log_random_k_ary = filtered_dataframe_log['param tree_type']=='random-k-ary'
    dataframe_log_random_k_ary = filtered_dataframe_log[log_random_k_ary]
    log_random = filtered_dataframe_log['param tree_type']=='random'
    dataframe_log_random = filtered_dataframe_log[log_random]
    log_random_flat = filtered_dataframe_log['param tree_type']=='random_flat'
    dataframe_log_random_flat = filtered_dataframe_log[log_random_flat]
    log_random_deep = filtered_dataframe_log['param tree_type']=='random_deep'
    dataframe_log_random_deep = filtered_dataframe_log[log_random_deep]


    if not os.path.exists('plots'):
        os.mkdir('plots')


    #lin plot, bal_k_ary, delprob
    cc = 0
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param seq del_prob'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[cc], label='del prob: '+str(key))
        cc = cc+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/PROB-nodecount_tree_to_runtime_opseq50_lin_bal_k_ary.png', format='png')

    #lin plot, const height, delprob
    cc = 0
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param seq del_prob'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[cc], label='del prob: '+str(key))
        cc = cc+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/PROB-nodecount_tree_to_runtime_opseq50_lin_const_height_k.png', format='png')

    #lin plot, random kary, delprob
    cc = 0
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param seq del_prob'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[cc], label='del prob: '+str(key))
        cc = cc+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/PROB-nodecount_tree_to_runtime_opseq50_lin_random_k_ary.png', format='png')

    #lin plot, random, delprob
    cc = 0
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param seq del_prob'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[cc], label='del prob: '+str(key))
        cc = cc+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/PROB-nodecount_tree_to_runtime_opseq50_lin_random.png', format='png')

    #lin plot, random flat, delprob
    cc = 0
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param seq del_prob'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[cc], label='del prob: '+str(key))
        cc = cc+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/PROB-nodecount_tree_to_runtime_opseq50_lin_random-flat.png', format='png')

    #lin plot, random, delprob
    cc = 0
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param seq del_prob'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[cc], label='del prob: '+str(key))
        cc = cc+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/PROB-nodecount_tree_to_runtime_opseq50_lin_random-deep.png', format='png')

    ################################################################################################

    #lin plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_lin.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[i], label=key)
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_lin.png', format='png')

    colourcount=0
    #lin plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_lin_bal_k_ary.png', format='png')

    #lin plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(0,0.010)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_lin_random_k_ary.png', format='png')

    #lin plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    plt.ylim(0,0.010)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_lin_const_height_k.png', format='png')

    #lin plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_lin_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_lin_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    plt.ylim(0,0.010)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_lin_random-flat-deep.png', format='png')

    #log plot
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(filtered_dataframe_log.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[i], label=key)
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_log.png', format='png')

    colourcount=0
    #log plot, balanced-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_bal_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='bal-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_log_bal_k_ary.png', format='png')

    #log plot, random-k-ary
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random_k_ary.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='rand-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_log_random_k_ary.png', format='png')

    #log plot, const-height-k
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_const_height_k.groupby(['param tree_type', 'param tree_param'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label='const h-k:'+str(key[1]))
        colourcount=colourcount+1
    #plt.ylim(-5,25)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_log_const_height_k.png', format='png')

    #log plot, random, random-flat, random-deep
    fig, ax = plt.subplots()
    for i, (key, grp) in enumerate(dataframe_log_random.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_flat.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    for i, (key, grp) in enumerate(dataframe_log_random_deep.groupby(['param tree_type'])):
        ax = grp.plot(ax=ax, kind='scatter', x='nodecount tree', y='runtime', c=COLORS[colourcount], label=key)
        colourcount=colourcount+1
    #plt.ylim(-0.05,0.06)
    plt.xlim(-5,1200)
    plt.subplots_adjust(right=0.75)
    plt.legend(loc='center left', fontsize=9.5, bbox_to_anchor=(1.0, 0.5))
    plt.savefig('plots/nodecount_tree_to_runtime_opseq50_log_random-flat-deep', format='png')
