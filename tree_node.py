3#!/usr/bin/python3
from graphviz import Graph
from copy import copy

import global_params
from collections import deque
from errors import *

""" Contains classes for representing and working with trees, forests and contexts."""


class ForestEnvironment:
    """ Class for tracking ids of nodes.
    
    Forest environments only give a way to track nodes and perform and
    provide operations on all nodes. There are no Operations between
    forest environments. Those are instead represented as operations
    between root nodes (which act as their induced subtrees in this
    module).

    :ivar dict _nodes:
        Dict mapping tree node id -> tree node.

    :ivar int _id_counter:
        Counter for generating unique ids in case ids are not provided.
    """
    def __init__(self):

        self._nodes = dict()
        self._node_id_counter = 0
        self._virtual_node_id_counter = 0
        self._hole_id_counter = 0

    def to_dot_graph(self, filename):
        """ Generates a file in which the graph of the tree is painted.

        :param str filename:
            String which represents the name of generated file.
        """
        if len(self.get_roots()) == 0:
            dot = Graph(filename)
            dot.render()
        else:
            graph = Graph(filename, filename, engine='dot')
            
            def postproc(tree_node, resultdict):
                virt_text = "Virtual\n" if tree_node._is_virtual else ''
                if global_params.ARG_VERBOSE_TREES:
                    node_text = f"{virt_text}{tree_node.label}\n {tree_node.id}\n c: {tree_node._is_context}, h: {tree_node._is_hole}"
                else:
                    node_text = f"{virt_text}{tree_node.label}\n id:{tree_node.id}"
                graph.node(tree_node.id,label=node_text)
                for child in tree_node.children:
                    childnode = resultdict[child.id]
                    graph.edge(tree_node.id, childnode)
                return tree_node.id

            result_of_dft = self._depth_first_traversal(postproc)
            graph.render()

    def get_roots(self):
        """ Returns all roots within the forest algebra using a linear search (expensive).
        
        :return list: 
            List of all roots within the forest algebra.
        """
        return [tree_node for tree_node in self._nodes.values() if tree_node.parent is None]

    def count_nodes(self):
        """ Counts non-virtual nodes in the forest environment.

        :return int:
            Number of non-virtual nodes.
        """
        return len([tree_node for tree_node in self._nodes.values() if not tree_node._is_virtual])
    
    def get_height(self, get_all_heights=False):
        """ Returns height of forest which is represented by the forest environment.

        :param bool get_all_heights:
            If set to True return a dictionary mapping tree node id -> height of induced subtree instead.
        
        :return int|dict:
            Height of forest. 0 on empty forests.
        """
        if not self._nodes:
            # empty forest
            return 0
        def postproc(tree_node, resultdict):
            height = max([resultdict[child.id] for child in tree_node.children] + [0]) + 1
            resultdict[tree_node.id] = height
            return height

        result_of_dft = self._depth_first_traversal(postproc)
        roots = self.get_roots()
        height = max([result_of_dft[root.id] for root in roots if root._is_virtual == False] + [0])
        if get_all_heights:
            return result_of_dft
        else:
            return height

    def _check_consistency(self):
        """Checks the forest environments for inconsistencies:
            - If all roots are really roots.
            - If all context roots have valid hole pointers.
            - If all nodes are children of their parents.
            - If all holes have parents.
        """
        for root in self.get_roots():
            if root.parent is not None:
                raise RuntimeError(f'Inconsistent Environment: Element in root list {root.id} has parents')
            if root._get_is_context() and root._hole == None:
                raise RuntimeError(f'Inconsistent Environment: Root {root.id} represents context but has no valid hole.')
        for node in self._nodes.values():
            if node.parent is not None and node not in node.parent.children:
                raise RuntimeError(f'Inconsistent Environment: Tree Node {node.id} is not child of its parents')
            if node._is_hole and node.parent is None:
                raise RuntimeError(f'Inconsistent Environment: Hole {node.id} has no parent')

    def _depth_first_traversal(self, postproc):
        """ Does a depth first traversal while applying the postproc function to every node.

        :param function(top_of_stack, resultdict) postproc:
            Callback function which will be called on every element
            after all its children were processed. The resultdict
            contains a mapping of tree node ids to results and is
            guaranteed to include the results of each of the nodes
            child ids.

        :return dict:
            The resultdict as described above.
        """
        resultdict = dict()
        roots = self.get_roots()
    
        if len(roots) == 0:
            raise RuntimeError('_depth_first_traversal: No forest exists.')

        for root in roots:
            stack = deque()
            marked = set()     # stores a node if all direct children are pushed on stack
            stack.append(root)

            if len(root.children)==0:
                result = postproc(root, resultdict)
                resultdict[root.id] = result
                continue

            while len(stack)>0:
                top_of_stack = stack[-1]
                if top_of_stack not in marked:           
                    while len(top_of_stack.children)>0:
                        left_most_child = top_of_stack.children[0]
                        for child in reversed(top_of_stack.children):
                            stack.append(child)
                        marked.add(top_of_stack)
                        top_of_stack = left_most_child
                    # top_of_stack is leaf node now
                    marked.add(top_of_stack)
                else:
                    result = postproc(top_of_stack, resultdict)
                    resultdict[top_of_stack.id] = result
                    stack.pop()

        return resultdict

    def __deepcopy__(self, memo):
        """ Copies the given ForestEnvironment.

        :param dict memo:
            See documentation of deepcopy().
            https://docs.python.org/3/library/copy.html?highlight=deepcopy#copy.deepcopy

        :return ForestEnvironment:
            Copy of the given ForestEnvironment.
        """
        node_mapping = dict()
        forest_env_copy = ForestEnvironment()
        forest_env_copy._node_id_counter = copy(self._node_id_counter)
        forest_env_copy._virtual_node_id_counter = copy(self._virtual_node_id_counter)
        forest_env_copy._hole_id_counter = copy(self._hole_id_counter)

        for node_id, node in self._nodes.items():
            node_copy = copy(node)
            assert(node_copy.id == node.id)
            forest_env_copy._nodes[node_id] = node_copy
            node_mapping[node_id] = node_copy
            node_copy.forest_env = forest_env_copy

        for node_id_copy, node_copy in forest_env_copy._nodes.items():
            #fix childpointer
            node_copy.children = [ node_mapping[child.id] for child in node_copy.children ]
            #fix parentpointer
            if node_copy.parent is not None:
                parent = node_copy.parent
                parent_copy = node_mapping[parent.id]
                node_copy.parent = parent_copy

        forest_env_copy_roots = forest_env_copy.get_roots()
        for node_id_copy, node_copy in forest_env_copy._nodes.items():
            #fix holepointer
            if node_copy._is_context :
                if node_copy in forest_env_copy_roots:
                    hole = node_copy._hole
                    hole_copy = node_mapping[hole.id]
                    node_copy._hole = hole_copy
                else:
                    node_copy._hole = None

        return forest_env_copy

    def run_operation_sequence(self, sequence):
        """ Runs a sequence of operations.
        
        :param list sequence:
            List of tuples. ((operation, target[, new_label,  new_id[, _left]]),...) Where 
                operation is "ins_leaf", "ins_sub", "delete", 
                target is the target id for the 3 operations, 
                new_label is the label for the new nodes (inserts only),
                new_id is the id for the new node (insert only), 
                insert_left toggles wether insert_leaf is inserted left or right of the target leaf (insert leaf only).
        """
        for op in sequence:
            if len(op) < 2:
                raise RuntimeError(f"invalid operation in sequeince: {op}")
            if op[0] == "delete":
                self.delete_leaf(str(op[1]))
            elif op[0] == "ins_sub":
                self.insert_subdivision(str(op[2]), target_id=str(op[1]), new_id=str(op[3]))
            elif op[0] == "ins_leaf":
                if len(op) < 5:
                    insert_left = None
                else:
                    insert_left = op[4]
                self.insert_leaf(str(op[2]), target_id=str(op[1]), new_id=str(op[3]), insert_left=insert_left)
                
    def insert_leaf(self, new_label, target_id=None, new_id=None, insert_left=False):
        """ Inserts a new leaf left or right of the target.

        :param str new_label:
            The label of the new leaf that should be created.

        :param str target_id:
            The new tree node will be inserted left of or right of the
            tree node with this id.

        :param str new_id:
            ID the newly created tree node should get.
        
        :param bool insert_left:
            If set to true the new node will be inserted left of the
            target, otherwise it will be inserted right of the target.
        
        """

        # check for valid target id should happen before creation of node
        if target_id not in self._nodes and not (target_id is None or target_id == 'None'):
            raise InvalidTargetIdError(f"Invalid insert operation: target tree_node {target_id} not found in forest environment.")
        
        new_leaf = TreeNode(self, new_label, id=new_id)

        if not(target_id is None or target_id == 'None'):
            target_node = self._nodes[target_id]
            # if the target node is a root a forest (using virtual nodes) might need to be created
            if target_node.parent is None:
                if insert_left:
                    new_leaf._oplus(target_node)
                else:
                    target_node._oplus(new_leaf)
                return
            target_node._insert_leaf(new_leaf, insert_left=insert_left)

        
    def insert_subdivision(self, new_label, target_id=None, new_id=None):
        if target_id not in self._nodes and not (target_id is None or target_id == 'None'):
            raise InvalidTargetIdError(f"Invalid insert operation: target tree_node {target_id} not found in forest environment.")

        new_leaf = TreeNode(self, new_label, id=new_id)
        
        if not(target_id is None or target_id == 'None'):
            target_node = self._nodes[target_id]
            target_node._insert_subdivision(new_leaf)
        
    def delete_leaf(self, target_id):
        if target_id not in self._nodes:
            raise InvalidTargetIdError(f"Invalid delete operation: target tree_node {target_id} not found in forest environment.")
        self._nodes[target_id]._delete()
    
class TreeNode:
    """ TreeNode represent nodes for the tree, contexts and the induced subtrees rooted in the node.

    Forests are represented as virtual tree nodes which have the roots of the forest in their children list.

    :ivar str label:
        Label of the node.

    :ivar str id:
        Id of the node. Is unique within the nodes forest envirionment
        and used to adress the node for operations.
    
    :ivar TreeNode parent:
        Parent of the node or None if the node is a root.

    :ivar list children:
        list of children of the node in order
    
    :ivar ForestEnvironment forest_env:
        Forest environment for the node, keeps track of the nodes id.

    :ivar bool _is_virtual:
        True if this node is a virtual root node.

    :ivar bool _is_context:
        True if the tree rooted at this node contains a hole. Might contain nonsense for non-root nodes.

    :ivar bool _is_hole:
        True if this tree node is to be treated as a hole.

    :ivar TreeNode _hole:
        Hole of the tree rooted at this node. Might contain nonsense for non-root nodes.
    """
    def __init__(self, forest_env, label, id=None, is_hole=False, is_virtual=False):
        """ Represent nodes for the tree, contexts and the induced subtrees rooted in the node.

        :param ForestEnvironment forest_env:
            Environment in which the tree node should be created.

        :param str label:
            Label for the node.

        :param str id:
            ID for the node. Needs to be unique within the
            ForestEnvironment. If no id is provided one will be
            generated.

        :param bool is_hole:
            Set to true if the node should represent a hole. 

        :param bool is_virtual:
            Set to true if the node should be virtual.
        """
        
        self._is_virtual = is_virtual
        self.label = label
        self.children = []
        self.parent = None
        self._is_context = False # Only saved in root
        self._is_hole = is_hole
        self._hole = None # Only points to the correct hole if the node is a root i.e. has no parents
        self.forest_env = forest_env

        # handle id
        if id is None or id == 'None':
            if is_hole:
                self.forest_env._hole_id_counter += 1
                id = '__gen_hole_' + str(self.forest_env._hole_id_counter)
            elif is_virtual:
                self.forest_env._virtual_node_id_counter += 1
                id = '__gen_virt_' + str(self.forest_env._virtual_node_id_counter)
            else:
                self.forest_env._node_id_counter += 1
                id = '__gen_' + str(self.forest_env._node_id_counter)
        else:
            id = str(id)
        if id in self.forest_env._nodes:
            raise RuntimeError(f"Cannot create leaf with ID '{id}' as it already exists")
        self.forest_env._nodes[id] = self
        self.id = id

    def _get_is_hole(self):
        """ Returns the hole of the tree, which is represented by self. Only works on roots.

        :return TreeNode:
            Hole of the tree.
        """
        if self.parent is not None:
            raise RuntimeError('getting holes only supported for root nodes')
        return self._is_hole

    def _get_is_context(self):
        """ Returns whether the tree, represented by this node, is a context or not. Only works on roots.

        :return bool:
            True if the tree is context, False otherwise.
        """
        if self.parent is not None:
            raise RuntimeError('getting is_context only supported for root nodes')
        return self._is_context

    def forest_equals(self, other_tree_node, return_witness=False):
        """ Checks if the forest/tree rooted at this node has the same structure as the other forest/tree.

        Two trees are regarded as having the same structure if they
        are isomorphic in regard to labels, parents and children (ids
        dont have to match). 

        Trees dont need to be from the same forest environment in order to be compared.

        Uses breadth-first traversal to create a mapping between the trees.

        :param ForestEnvironment other_forest_env:
            Forest environment to compare to.

        :return bool:
            True if both environments match, False if they dont.
            If return_witness is set to True a 2-tuple of witnesses for unequality.
        """
        
        self_to_other_map = dict()
        first_queue = deque([self])
        second_queue = deque([other_tree_node])
        while(first_queue or second_queue):
            # if one forest is done and the other isnt, return an element without a mapping
            if not (second_queue):
                return (first_queue.pop(), None) if return_witness else False
            if not (first_queue):
                return (None, second_queue.pop()) if return_witness else False
            
            first_element = first_queue.pop()
            second_element = second_queue.pop()
            false_result = (first_element, second_element) if return_witness else False
            self_to_other_map[first_element] = second_element
            
            # compare parents
            if (first_element.parent in self_to_other_map): #skip check for the subtree roots
                # parents of mapped nodes need to map as well.
                if self_to_other_map[first_element.parent] is not second_element.parent:
                    return false_result

            # compare labels
            if first_element.label != second_element.label:
                return false_result

            for f_child in first_element.children:
                first_queue.append(f_child)
            for s_child in second_element.children:
                second_queue.append(s_child)

        return (None, None) if return_witness else True
        
    def _to_context_leaf(self):
        """ Creates a new hole below the leaf. 

        Only valid if the leaf is a root to help prevent cases where there are multiple holes in a tree.
        """
        if self.parent is not None:
            raise RuntimeError('Insertion of holes is only allowed for leaves which dont have parents yet')
        if len(self.children) != 0:
            raise RuntimeError('Insertion of holes is only allowed for leaves')
        hole_node = TreeNode(self.forest_env, 'box', is_hole=True)
        self.children = [hole_node]
        self._is_context = True
        hole_node.parent = self
        self._hole = hole_node

    def _insert_leaf(self, tree_node, insert_left):
        """ Inserts a new tree leaf next to this tree node.

        :param TreeNode tree_node: 
            The new tree node.

        :param bool insert_left: 
            True if the new tree node should be inserted left of this
            tree node, False if it should be inserted right of this
            tree node.
        """
        if (self is None) or (self.id not in self.forest_env._nodes):
            raise RuntimeError('Insert child: Tree node does not exist.')
        if self._is_virtual:
            raise RuntimeError('Insert child: Tree node is not a node (but virtual node).')
        if tree_node._is_hole:
            raise RuntimeError('Insert child: Cannot insert a hole.')
        if len(tree_node.children) != 0:
            raise RuntimeError('Insert child: Tree node is not a leaf')
                
        parent_self = self.parent
        pos_of_self = parent_self.children.index(self)
        if insert_left:
            parent_self.children.insert(pos_of_self, tree_node)
        elif insert_left == False:
            parent_self.children.insert(pos_of_self+1, tree_node)
        tree_node.parent = parent_self

    def _insert_subdivision(self, tree_node):
        """ Inserts a new tree node below this node, all children of this node now become childern of the new tree node.

            :param TreeNode tree_node:
                The tree node to be inserted.
        """
        if (self is None) or (self.id not in self.forest_env._nodes):
            raise RuntimeError('Subdiv: Tree node does not exist.')
        if self._is_virtual:
            raise RuntimeError('Subdiv: Tree node is not a node (but virtual node).')
        if tree_node._is_hole:
            raise RuntimeError('Subdiv: The new tree node is a hole.')

        tree_node.children = self.children.copy()
        self.children.clear()
        self.children.append(tree_node)
        tree_node.parent = self
        for elem in tree_node.children:
            elem.parent = tree_node 

        self.forest_env._nodes[tree_node.id] = tree_node

    def _delete(self):
        """ Deletes this node if it is a leaf.
        """
        if (self is None) or (self.id not in self.forest_env._nodes):
            raise RuntimeError('Delete: Tree node does not exist.')
        if self._is_virtual:
            raise RuntimeError('Delete: Tree node is not a node (but virtual node).')
        if len(self.children) != 0:
            raise RuntimeError(f'Delete: Cannot delete treenode {self.label}: Treenode is not a leaf')

        # if tree node is root no childpointer has to be updated
        if self.parent is None:
            self.forest_env._nodes.pop(self.id)
            return
        
        parent_self = self.parent
        
        # if tree node is last or second last child of virtual root node: delete virtual node too
        if self.parent._is_virtual and len(self.parent.children) <= 2:
            # in case a sbiling exists it will become a (non-virtual) root.
            for selfandsibling in self.parent.children:
                selfandsibling.parent = None
            parent_self._children = None
            self.forest_env._nodes.pop(parent_self.id)
            self.forest_env._nodes.pop(self.id)
            return

        # standard case for regular leaf

        if self in parent_self.children:
            parent_self.children.remove(self)
        parent_self.forest_env._nodes.pop(self.id)
        self.parent = None

    def _relabel(self, new_label):
        """ Relables a tree node.

        :param str new_label:
            New label of the tree_node.
        """
        if (self is None) or (self.id not in self.forest_env._nodes):
            raise RuntimeError('Relabel: Tree node does not exist.')
        if self._is_virtual:
            raise RuntimeError('Relabel: Tree node is not a node (but virtual node).')
        if new_label is None:
            raise RuntimeError('Relabel: new_label is None.')
        self.label = new_label

    def _oplus(self, right_operand):
        """ Does a horizontal concatenation.

        Only works between root nodes. The data structures are modified in place, and the root of the resulting forest is returned. 

        Oplus concatenates the right operands roots to the left operands roots.
 
        :param TreeNode right_operand:
            Root node of the forest/tree that should be oplus'ed to this tree_node.

        :return TreeNode:
            Root of the resulting forest.
        """
        #check validity of operands
        if self._get_is_context() and right_operand._get_is_context():
            raise RuntimeError('Cannot oplus {self.id} with {right_operand.id}: Cannot oplus two contexts')
        
        if self.parent is not None or right_operand.parent is not None:
            raise RuntimeError('Oplus: At least one of the operands is not a root')

        if self.forest_env is not right_operand.forest_env:
            raise RuntimeError('Cannot oplus two trees from different environments')
       
        #Case 1: tree oplus tree
        if not self._is_virtual and not right_operand._is_virtual:
            env = self.forest_env
            virtual_root = TreeNode(env, "virtual", is_virtual=True) #also adds virtual_node to _nodes
            virtual_root.children.append(self)
            virtual_root.children.append(right_operand)
            
            # Delete self and right_operands as roots
            # Move their information about _is_context and _hole to virtual_root
            if self._get_is_context() and not right_operand._get_is_context():
                virtual_root._is_context = True
                virtual_root._hole = self._hole
                self._is_context = False
                self._hole = None
            elif right_operand._get_is_context() and not self._get_is_context():
                virtual_root._is_context = True
                virtual_root._hole = right_operand._hole
                right_operand._is_context = False
                right_operand._hole = None
           
            self.parent = virtual_root
            right_operand.parent = virtual_root

            return virtual_root

        #Case 2: forest oplus tree
        elif self._is_virtual and not right_operand._is_virtual:
            #self is a virtual node und will become parent of right_operand too
            self.children.append(right_operand)

            #Delete right_operand as root and move its information about _is_context and _hole to self
            if right_operand._get_is_context():
                self._is_context = True
                self._hole = right_operand._hole
                right_operand._is_context = False
                right_operand._hole = None

            right_operand.parent = self

            return self

        #Case 3: tree oplus forest
        elif not self._is_virtual and right_operand._is_virtual:
            #right_operand is a virtual node and will become parent of left operand. Left operand moves to front of childlist
            right_operand.children.insert(0, self)

            #Delete left_operands as roots and move its information about _is_context and _hole to right_operand
            if self._get_is_context():
                right_operand._is_context = True
                right_operand._hole = self._hole
                self._is_context = False
                self._hole = None

            self.parent = right_operand
           
            return right_operand

        #Case 4: forest oplus forest
        elif self._is_virtual and right_operand._is_virtual:
            #self is a virtual node und will become parent of all children of right_operand
            for child in right_operand.children:
                self.children.append(child)
                child.parent = self

            #Move information in right_operand about _is_context and _hole to self
            if right_operand._get_is_context():
                self._is_context = True
                self._hole = right_operand._hole
                right_operand._is_context = False
                right_operand._hole = None

            # remove right virtual node
            self.forest_env._nodes.pop(right_operand.id)
                
            return self

        raise RuntimeError('Oplus: no case matched.')


    def _odot(self, right_operand):
        """ Does vertical concatenation.

        Only works between root nodes. The data structures are modified in place, and the root of the resulting forest is returned. 

        Odot inserts the right operands roots into the left operands hole.
 
        :param TreeNode right_operand:
            Root node of the forest/tree that should be odotted'ed to this tree_node.

        :return:
            Root of the resulting forest.
        """
        if not self._get_is_context():
            raise RuntimeError(f'Odot: Cannot odot {self.id} with {right_operand.id}: First operand of odot has to be a context')
        if self.parent is not None or right_operand.parent is not None:
            raise RuntimeError('Odot: At least one of the operands is not a root')
        if self.forest_env is not right_operand.forest_env:
            raise RuntimeError('Odot: Cannot odot two trees from different environments')

        # case * odot tree
        if not right_operand._is_virtual:
            # replace hole with right operand
            hole_parent = self._hole.parent
            hole_parent.children = [right_operand if child == self._hole else child for child in hole_parent.children]
            right_operand.parent = hole_parent
            self._is_context = False
            self._hole._delete()
            
            # update left operands hole
            if right_operand._is_context:
                self._hole = right_operand._hole
                right_operand._hole = None
                self._is_context = right_operand._is_context

            return self
        # case * odot forest
        else:
            # replace hole with right operands children
            hole_parent = self._hole.parent
            for child in right_operand.children:
                child.parent = hole_parent
            hole_index = hole_parent.children.index(self._hole)
            hole_parent.children = hole_parent.children[0:hole_index] + right_operand.children + hole_parent.children[hole_index + 1:]
            self._is_context = False
            self._hole._delete()

            # update left operands hole
            if right_operand._is_context:
                self._hole = right_operand._hole
                right_operand._hole = None
                self._is_context = right_operand._is_context

            # right operand virtual node no longer needed
            self.forest_env._nodes.pop(right_operand.id)

            return self
        raise RuntimeError('Odot: no case matched.')
