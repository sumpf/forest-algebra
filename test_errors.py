#!/usr/bin/python3
from errors import *
def test_errors():
    # invalid target id
    try:
        raise InvalidTargetIdError("test")
    except InvalidTargetIdError:
        pass

    # duplicate id
    try:
        raise DuplicateIdError("test")
    except DuplicateIdError:
        pass

    # inconsistent formula
    try:
        raise InconsistentFormulaError("test")
    except InconsistentFormulaError:
        pass

    # wrong context
    try:
        raise WrongContextError("test")
    except WrongContextError as error:
        assert isinstance(error, InconsistentFormulaError)

    # wrong operation
    try:
        raise WrongOperationError("test")
    except WrongOperationError as error:
        assert isinstance(error, WrongOperationError)

    # wrong balance
    try:
        raise WrongBalanceError("test")
    except WrongBalanceError as error:
        assert isinstance(error, WrongBalanceError)
    
    
