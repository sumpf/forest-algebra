#!/usr/bin/python3
from graphviz import Graph

import global_params
from errors import *
"""Contains the classes for representing nodes of forest algebra formulas"""

class _Node:
    """ Base class for representing nodes of forest algebra formula 

    Use _Inner_Node and _Leaf when instantiating.

    :param Formula formula:
       formula in which the node is created.
    """
    def __init__(self, formula, is_context=None):
        assert isinstance(self, _Leaf) or isinstance(self, _InnerNode)
        
        self._formula = formula
        self._is_context = is_context
        self._parent = None
        self._signatures = None
        self._left_depth = None
        self._right_depth = None
        self._balance_factor = None

    def _getSibling(self):
        """ Returns the nodes unique sibling or None if no parent exists.

        Every node except the root node has a sibling.

        :return _Node:
            Sibling node of given node.
        """
        if self._parent is None:
            return None

        if self._parent._right_child == self:
            return self._parent._left_child
        elif self._parent._left_child == self:
            return self._parent._right_child
        
        raise InconsistentFormulaError(f"Node {self.id} is not the child of its parent.")

    def _update_balance(self, only_check=False):
        """ Recalculates balance factor and left and right depth. 
        
        :param bool only_check:
            If set to true dont update balance but raise WrongBalanceError if balance is incorrect.
        """
        #Leaves dont need operations, _InnerNode overwrites.
        return

    def _update_is_context(self, only_check=False):
        #Leaves dont need operations, _InnerNode overwrites.
        return

    def _update_signatures(self, leaf_signature_mapping, only_check=False):
        """ Sets the signatures to the leaf.
            Calculates and sets the signatures for inner node based on the signatures of the children.

        :param dict leaf_signature_mapping:
            Contains a mapping for each symbol to the signatures a leaf with that symbol should get.
            Is part of the nfta which has been set to the formula.
        :param bool only_check:
            If set to true dont update, but raise an Error in case the signature is wrong.
        """

        if leaf_signature_mapping is None:
            raise RuntimeError(f'Cannot update signatures: no leaf_signature_mapping given.')

        new_signatures = None

        # node is _Leaf
        if isinstance(self, _Leaf):
            if (self._label, self._is_context) not in leaf_signature_mapping:
                raise WrongAutomatonError(f"Missing signature mapping from ({self._label},{self._is_context}) to signatures")
            new_signatures = leaf_signature_mapping[(self._label, self._is_context)]            
            if only_check and self._signatures != new_signatures:
                raise WrongContextError(f"""Node {self.id} has wrong
                                       _signatures Attribute:{self._signatures} but
                                       should be {new_signatures}""") 
            else:
                self._signatures = new_signatures
            if not isinstance(self._signatures, set):
                raise WrongAutomatonError(f"Something went wrong: leaf {self.id} was assigned a wrong signature set.")
            return
        
        # node is _InnerNode
        if self._left_child._signatures is None or self._right_child._signatures is None:
            raise RuntimeError(f'Cannot update signatures of node {self.id}: signatures for its children are missing.')
        signature_result = set()
        # Case oplus_HH
        if self._operation == 'oplus' and not self._left_child._is_context and not self._right_child._is_context:
            for (q_1,q_2) in self._left_child._signatures:
                for (q_3, q_4) in self._right_child._signatures:
                    if q_3 == q_4:
                        if only_check and (q_1,q_4) not in self._signatures:
                            raise WrongContextError(f"""Node {self.id} has wrong
                                        _signatures Attribute:{self._signatures} 
                                        should contain {(q_1,q_4)}""") 
                        else:
                            signature_result.add((q_1,q_4))
                        
        # Case oplus_VH
        elif self._operation == 'oplus' and self._left_child._is_context and not self._right_child._is_context:
            for (tuple_q1_q2, tuple_q3_q4) in self._left_child._signatures:
                for (q5, q6) in self._right_child._signatures:
                    (q1, q2) = tuple_q1_q2
                    if q2 == q5:
                        if only_check and ((q1,q6),tuple_q3_q4) not in self._signatures:
                            raise WrongContextError(f"""Node {self.id} has wrong
                                        _signatures Attribute:{self._signatures} 
                                        should contain {((q1,q6),tuple_q3_q4)}""") 
                        else:
                            signature_result.add(((q1,q6),tuple_q3_q4))
        # Case oplus_HV
        elif self._operation == 'oplus' and not self._left_child._is_context and self._right_child._is_context:
            for (q1,q2) in self._left_child._signatures:
                for (tuple_q3_q4, tuple_q5_q6) in self._right_child._signatures:
                    (q3, q4) = tuple_q3_q4
                    if q_2 == q3:
                        if only_check and ((q1,q4),tuple_q5_q6) not in self._signatures:
                            raise WrongContextError(f"""Node {self.id} has wrong
                                        _signatures Attribute:{self._signatures} 
                                        should contain {((q1,q4),tuple_q5_q6)}""") 
                        else:
                            signature_result.add(((q1,q4),tuple_q5_q6))
            
        # Case oplus_VV
        elif self._operation == 'oplus' and self._left_child._is_context and self._right_child._is_context:
            raise WrongOperationError(f"Error in operation of Node {self.id}: Cannot oplus two contexts")
        
        # Case odot_VV
        elif self._operation == 'odot' and self._left_child._is_context and self._right_child._is_context:
            for (tuple_q1_q2, tuple_q3_q4) in self._left_child._signatures:
                for (tuple_q5_q6, tuple_q7_q8) in self._right_child._signatures:
                    if tuple_q3_q4 == tuple_q5_q6:
                        if only_check and (tuple_q1_q2, tuple_q7_q8) not in self._signatures:
                            raise WrongContextError(f"""Node {self.id} has wrong
                                        _signatures Attribute:{self._signatures} 
                                        should contain {(tuple_q1_q2, tuple_q7_q8)}""") 
                        else:
                            signature_result.add((tuple_q1_q2, tuple_q7_q8))
            
        # Case odot_VH
        elif self._operation == 'odot' and self._left_child._is_context and not self._right_child._is_context:
            for (tuple_q1_q2, tuple_q3_q4) in self._left_child._signatures:
                for (q5, q6) in self._right_child._signatures:
                    if tuple_q3_q4 == (q5, q6):
                        if only_check and tuple_q1_q2 not in self._signatures:
                            raise WrongContextError(f"""Node {self.id} has wrong
                                        _signatures Attribute:{self._signatures} 
                                        should contain {tuple_q1_q2}""") 
                        else:
                            signature_result.add(tuple_q1_q2)
            
        # Case odot_H?
        elif self._operation == 'odot' and not self._left_child._is_context:
            raise WrongOperationError(f"Error in operation of Node {self.id}: Cannot oplus two contexts")
        
        else:
            raise WrongOperationError(f"Error in operation of Node {self.id}: Unknown operation or operands")

        self._signatures = signature_result

class _InnerNode(_Node):
    def __init__(self, formula, is_context=None, left_child=None, right_child=None, operation=None, id=None):
        """ Class for representing inner nodes of a forest algebra formula.

        :param Formula formula:
            Formula in which the node is created.

        :param bool is_context:
            True if the subformula rooted in this node evaluates to a
            context, False if it results in a forest.

        :param _Node left_child:
            Left operand of the operation.

        :param _Node right_child:
            Right operand of the operation.

        :param "oplus"|"odot" operation:
            Plus refers to horizontal concatenation, dot to vertical
            concatenation/context application.

        :param int balance_factor:
            Height difference between the right subtree and the left
            subtree. Positive numbers mean the right subtree is higher.
        """
        super().__init__(formula, is_context=is_context)
        self._left_child = left_child
        self._right_child = right_child

        assert operation in ['oplus','odot']
        self._operation = operation

        # generate id, ids for inner_nodes are seperate from leaf ids.
        if id is None:
            formula._inner_node_id_counter += 1
            id = '__gen_inner_' + str(formula._inner_node_id_counter)
        else:
            id = str(id)
        # check for duplicate id
        if id in formula._nodes:
            raise DuplicateIdError(f"Cannot create leaf with ID '{id}' as it already exists")
        
        formula._nodes[id] = self
        formula._inner_nodes[id] = self
        self.id = id

        # set as parent of its children
        if left_child is not None and right_child is not None:
            left_child._parent = self
            right_child._parent = self

        if self._left_child is not None and self._right_child is not None:
            # calculate balance if children were set
            self._update_balance()
            # calculate _is_context if children were set. If _is_context is provided check it instead.
            self._update_is_context(only_check=(is_context is not None))

    def _to_dot_graph(self, graph):
        """ Adds this node, its children, and the subtree to the provided graph. 

        :param Graph graph: 
            Graph in which the nodes are inserted.

        :return Graph:
            Id of given node.
        """
        #generate nodes for children first
        left_node_id = self._left_child._to_dot_graph(graph)
        right_node_id = self._right_child._to_dot_graph(graph)
        #create node
        if global_params.ARG_VERBOSE_TREES:
            node_text=f"""{self._operation}(id:{self.id})\n B:{str(self._balance_factor)}, c:{str(self._is_context)}\nld:{str(self._left_depth)}, rd:{str(self._right_depth)}\n Sig: {str(self._signatures)}"""
        else:
            node_text=f"{self._operation}\nB:{str(self._balance_factor)}"
        graph.node(self.id, label=node_text)
        #connect to children
        graph.edge(self.id, left_node_id)
        graph.edge(self.id, right_node_id)
        return self.id

    def _change_children(self, new_left_child, new_right_child):
        """ Changes the children of the node, handling a few things that could go wrong.

        Changes the children of the node, then uses the types of the
        children to update the type of the node, e.g. a forest oplus a
        context results in a context. Raises WrongOperationError Exceptions in invalid
        cases, e.g. context oplus context. Also removes the parent
        assignment of old children.

        :param _Node new_left_child: 
            New left child.
        :param _Node new_right_child: 
            New right child.
        """
        # handle old children
        if self._left_child is not None:
            self._left_child._parent = None            
        if self._right_child is not None:
            self._right_child._parent = None

        # handle old parents of new children
        if new_left_child and new_left_child._parent and new_left_child._parent._left_child == new_left_child:
            new_left_child._parent._left_child = None
        elif new_left_child and new_left_child._parent:
            new_left_child._parent._right_child = None
        if new_right_child and new_right_child._parent is not None and new_right_child._parent._right_child == new_right_child:
            new_right_child._parent._right_child = None
        elif new_right_child and new_right_child._parent:
            new_right_child._parent._left_child = None
            
        # handle new children
        self._left_child = new_left_child
        self._right_child = new_right_child
        if new_left_child:
            new_left_child._parent = self
        if new_right_child:
            new_right_child._parent = self
        # check operation
        self._update_is_context()
        self._update_balance()
        if self._formula._nfta is not None:
            self._update_signatures(self._formula._nfta._leaf_signature_mapping)

    def _update_is_context(self, only_check=False):
        """ Recalculates wether the node represents a context. 
    
        Checks both children and the operation and updates the _is_context field.

        :param bool only_check:
            If set to true dont update, but raise an WrongContextError in case the field is wrong.
        """
        new_is_context = None
        if self._operation == "oplus":
            if self._left_child._is_context and self._right_child._is_context:
                # case c oplus c
                raise WrongOperationError(f"""Invalid operands after changing
                                              children of Node '{self.id}':
                                              Cannot oplus two contexts.""")
            #change type
            elif self._left_child._is_context or self._right_child._is_context:
                # cases c oplus f/f oplus c
                new_is_context = True
            else:
                # case f oplus f
                new_is_context = False
        if self._operation == "odot":
            #handle invalid case
            if not self._left_child._is_context:
                # cases f odot c/f odot f
                raise WrongOperationError(f"""Invalid operands after
                                              changing children of
                                              Node '{self.id}': First
                                              operand of odot has to
                                              be a context.""")
            elif self._right_child._is_context:
                # case c odot c
                new_is_context = True
            else:
                # case c odot f
                new_is_context = False
        
        if only_check and self._is_context != new_is_context:
            raise WrongContextError(f"""Node {self.id} has wrong
                                   _is_context
                                   Attribute:{self._is_context} but
                                   should be {new_is_context}""")
        else:
            self._is_context = new_is_context

    def _update_balance(self, only_check=False):
        """ Recalculates balance factor and left and right depth based on the depths of the children.
        
        :param bool only_check:
            If set to true dont update balance but raise an exception if balance is incorrect.
        """
        new_left_depth = max(self._left_child._left_depth, self._left_child._right_depth)+1
        new_right_depth = max(self._right_child._left_depth, self._right_child._right_depth)+1
        new_balance = new_right_depth - new_left_depth
        
        if only_check and (new_left_depth != self._left_depth
                           or new_right_depth != self._right_depth
                           or new_balance != self._balance_factor):
            raise WrongBalanceError(f"invalid balance/depth detected at node {self.id}")
        
        self._left_depth = new_left_depth
        self._right_depth = new_right_depth
        self._balance_factor = new_balance

    def _delete(self):
        """ Deletes the node from its formula and from the children of its parent and from the 
            parents of its children.
        """
        #fix parents
        if self._parent is not None:
            if self._parent._left_child == self:
                self._parent._left_child = None
            else:
                self._parent._right_child = None

        #fix children
        if self._right_child is not None and self._right_child._parent == self:
            self._right_child._parent = None
        if self._left_child is not None and self._left_child._parent == self:
            self._left_child._parent = None
        
        #remove from nodes
        self._formula._nodes.pop(self.id)
        self._formula._inner_nodes.pop(self.id)
    
    
class _Leaf(_Node):
    def __init__(self, formula, label, is_context=False, id=None):
        """ Class for representing leaf nodes of a forest algebra formula.

        :param Formula formula:
            Formula in which the node is created.

        :param str label:
            Symbol represented in the leaf.

        :param bool is_context:
            True if the leaf represents a symbol with attached box, e.g. a_Box, false if it is just the symbol.

        :param str id:
            Id of the leaf. This is used to identify the leaf for insert/delete/relabel operations
        """
        super().__init__(formula, is_context=is_context)
        
        assert isinstance(label, str), "label needs to be string"
        self._label = label
        # following values are always 0 for leaves
        self._left_depth = 0
        self._right_depth = 0
        self._balance_factor = 0

        # generate random id if needed
        if id is None or id == 'None':
            formula._leaf_id_counter += 1
            id = '__gen_' + str(formula._leaf_id_counter)
        else:
            id = str(id)

        # check for duplicate ids
        if id in formula._nodes:
            raise DuplicateIdError(f"Cannot create leaf with ID '{id}' as it already exists")
        
        formula._nodes[id] = self
        formula._leaves[id] = self
        self.id = id

    def _relabel(self, new_label):
        """ Relabels the leaf node.

        :param str new_label:
            New label of the node.
        """
        self._label = new_label

    def _to_dot_graph(self, graph):
        """ Adds this node, its children, and the subtree to the provided graph. 

        :param Graph graph: 
            Graph in which the nodes are inserted.

        :return Graph:
            Id of given node.
        """
        # create node
        if global_params.ARG_VERBOSE_TREES:
            node_text=f"""{self._label}(id:{self.id})\n B:{str(self._balance_factor)}, c:{str(self._is_context)}\nld:{str(self._left_depth)}, rd:{str(self._right_depth)}\n Sig: {str(self._signatures)}"""
        else:
            node_text=f"{self._label}\nid:{self.id}"
        
        graph.node(self.id, graph.node(self.id, label=node_text))
        return self.id

    def _delete(self):
        """ Deletes the node from its formula and from the children of its parent."""
        if self._parent is not None:
            if self._parent._left_child == self:
                self._parent._left_child = None
            else:
                self._parent._right_child = None
        self._formula._nodes.pop(self.id)
        self._formula._leaves.pop(self.id)

