#!/usr/bin/python3
from rotations import rotate
import node
from node import _InnerNode, _Leaf
from formula import Formula
from errors import *
import os

def test_output():
    # test for output which directly creates node and leaf structure without using insert operations
    print('test_output:creating simple tree')
    formula = Formula()
    leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_ab')
    leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
    node_ab = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=leaf_b, operation='odot')
    #leaf_a2 = _Leaf(formula, 'a', is_context=False, id='leaf_a')
    leaf_c = _Leaf(formula, 'c', is_context=False, id='leaf_c')
    #node_ac = _InnerNode(formula, is_context=False, left_child=leaf_a2, right_child=leaf_c, operation='oplus')
    node_abac = _InnerNode(formula, is_context=False, left_child=node_ab, right_child=leaf_c, operation='odot')
    formula.root = node_abac
    formula.to_dot_graph("test_output_graph")
    os.remove("test_output_graph.gv")
    os.remove("test_output_graph.gv.pdf")

def test_output_empty_graph():
    formula = Formula()
    formula.to_dot_graph("test_empty_output_graph")
    os.remove("test_empty_output_graph.gv")
    os.remove("test_empty_output_graph.gv.pdf")

def test_bottom_up_pass_father_not_child_of_parent():
    error_thrown = False
    try:
        formula = Formula()
        leaf_a = _Leaf(formula, 'a_box', is_context=True, id='leaf_ab')
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        node_ab = _InnerNode(formula, is_context=True, left_child=leaf_a, right_child=leaf_b, operation='odot')
        node_ab._left_child = node_ab
        def nop(node, left_result=None, right_result=None):
            pass
        formula._bottom_up_pass_all(nop)
    except InconsistentFormulaError:
        error_thrown = True
    assert error_thrown

def test_bottom_up_pass_detect_circle():
    error_thrown = False
    try:
        formula = Formula()
        leaf_b = _Leaf(formula, 'b_box', is_context=True, id='leaf_bb')
        node_ab = _InnerNode(formula, is_context=True, left_child=None, right_child=leaf_b, operation='odot')
        node_ab._left_child = node_ab
        node_ab.parent = node_ab
        def nop(node, left_result=None, right_result=None):
            pass
        formula._bottom_up_pass_all(nop)
    except InconsistentFormulaError:
        error_thrown = True
    assert error_thrown
