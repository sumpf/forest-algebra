#!/usr/bin/python3
from formula import Formula
from tree_node import ForestEnvironment

sequences = {
    # a sequence of some simple inserts
    'insert_sequence':[("ins_leaf", None, "0", "0"),#insert roots insert same target chain
                       ("ins_leaf", "0", "1", "1"),
                       ("ins_leaf", "0", "2", "2", True),
                       ("ins_leaf", "0", "3", "3", False),
                       #insert subdivision vertical chain
                       ("ins_sub", "0", "4","4"),
                       ("ins_sub", "4", "5","5"),
                       ("ins_sub", "5", "6","6"),
                       ("ins_sub", "6", "7","7"),
                       ("ins_sub", "7", "8","8"),
                       #insert subdivision same target chain
                       ("ins_sub", "7", "9","9"),
                       ("ins_sub", "7", "10","10"),
                       ("ins_sub", "7", "11","11"),
                       ("ins_sub", "7", "12","12"),
                       ("ins_sub", "7", "13","13")],
    # a sequence which at one point has to remove virtual nodes and turn a forest into a tree
    'forest_to_tree_sequence':[('ins_leaf', None, 0, 0, False),
                               ('ins_leaf', '0', 2, 2, False),
                               ('ins_sub', '2', 3, 3),
                               ('delete', '3'),
                               ('ins_sub', '0', 5, 5),
                               ('ins_sub', '0', 6, 6),
                               ('ins_leaf', '6', 7, 7, True),
                               ('ins_leaf', '2', 8, 8, True),
                               ('ins_leaf', '2', 10, 10, False),
                               ('delete', '8'),
                               ('ins_sub', '7', 12, 12),
                               ('ins_leaf', '6', 13, 13, True),
                               ('delete', '2'),
                               ('ins_sub', '5', 15, 15),
                               ('ins_leaf', '7', 16, 16, False),
                               ('ins_sub', '5', 18, 18),
                               ('ins_leaf', '5', 19, 19, True),
                               ('ins_leaf', '15', 20, 20, False),
                               ('delete', '13'),
                               ('delete', '10'),
                               ('ins_leaf', '5', 23, 23, True),
                               ('delete', '23'),
                               ('ins_leaf', '20', 25, 25, False),
                               ('ins_leaf', '25', 26, 26, True),
                               ('ins_sub', '26', 27, 27),
                               ('ins_sub', '6', 28, 28),
                               ('ins_sub', '0', 29, 29)]}

def test_sequence_formula_consistency():
    for sequence_name, op_sequence in sequences.items():
        formula = Formula()
        try:
            for op in op_sequence:
                formula.run_operation_sequence([op])
                formula.check_consistency()
        except Exception as error:
            formula.to_dot_graph('FAIL_test__sequence_formula_consistency_{sequence_name}')
            raise error

def test_sequence_forest_env_consistency():
    for sequence_name, op_sequence in sequences.items():
        forest_env = ForestEnvironment()
        try:
            for op in op_sequence:
                forest_env.run_operation_sequence([op])
                forest_env._check_consistency()
        except Exception as error:
            forest_env.to_dot_graph('FAIL_test_sequence_forest_env_consistency_{sequence_name}')
            raise error
        
def test_sequence_equality():
    for sequence_name, op_sequence in sequences.items():
        forest_env_1 = ForestEnvironment()
        formula = Formula()
        for op in op_sequence:
            forest_env_2 = ForestEnvironment()
            try:
                forest_env_1.run_operation_sequence([op])
                formula.run_operation_sequence([op])
                formula.to_tree(forest_env_2)
                witness = forest_env_2.get_roots()[0].forest_equals(forest_env_1.get_roots()[0], return_witness=True)
                assert witness == (None, None), f'Forest environments inequal. Witnesses ids:{[tree_node.id for tree_node in witness]}'
            except Exception as error:
                forest_env_1.to_dot_graph('FAIL_test_sequence_equality_to_tree_{sequence_name}')
                forest_env_2.to_dot_graph('FAIL_test_sequence_equality_tree_{sequence_name}')
                formula.to_dot_graph('FAIL_test_sequence_equality_formula_{sequence_name}')
                raise error
                
